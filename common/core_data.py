# coding=utf-8
"""
general data that should be defined package wide
"""

import importlib
import logging
import os
import sys

from typing import Type, Union

# common paths
from common.plugins_core import PluginsCore, PluginsCoreGUI

FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)
CONFIG_PATH = os.path.join(os.path.expanduser("~/.config"), os.path.splitext(FILENAME_SCRIPT)[0])
if CONFIG_PATH.endswith("_service"):
    CONFIG_PATH = CONFIG_PATH[:-8]

# subfolder
SERVICE_FOLDER = "services"
PLUGIN_FOLDER = "plugins"

#
SECTION_KEYBOARD_LED = "keyboard_led"
SUB_SECTION_DISABLED = "disabled_plugins"
SUB_SECTION_CHECK_SAVED = "check_on_save"
SUB_SECTIONS = [SUB_SECTION_CHECK_SAVED, SUB_SECTION_DISABLED]


def init_class(plugin_name: str, exten: str = "") -> \
    Union[Type[PluginsCore], Type[PluginsCoreGUI], None]:
    """
    load and initialise objects from the plugin folder.

    This loads the plugin named by plugin_name; The classname inside of the
    pluginfile is a capitalized string of the plugin filename.
    E.g. mail_watcher -> MailWatcher; git_alert -> GitAlert, profile -> Profile

    Every plugin class needs a ```__init__()``` function that get the serviceTalker and the
    config section for the plugin.

    Parameters
    ----------
    plugin_name : str
        name of the plugin to load
    exten : str
        extension for class to load

    Return
    ------
    object
        the loaded object
    """
    objectname = ''.join(x.capitalize() or '_' for x in plugin_name.split('_'))
    try:
        obj = importlib.import_module("%s.%s" % (PLUGIN_FOLDER, plugin_name))
    except AttributeError:
        logging.error("Plugin named '%s' not loaded!", plugin_name)
        return None
    class_name = "%s%s" % (objectname, exten)
    if hasattr(obj, class_name):
        return getattr(obj, class_name)
    return None
