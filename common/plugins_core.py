#!/usr/bin/env python3
# coding=utf-8
"""
base class for plugins
"""

import glob
import json
import logging
import os
import sys
import threading

from typing import Any, Dict, List, Optional, Union

from common.ExColourPickerCtrl import ExColourPickerCtrl
from services.service_talker import ServiceTalkerBase


def preferences_struct():
    """
    json preferences structure for the plugin

    Return
    ------
    structure of the config file entry
    """
    return {}

# this is necessary for wx, sorry
# pylint: disable=no-member,too-many-arguments
class PluginsCore(threading.Thread):
    """
    base class for plugins
    """
    i_time_slice = 0.1  # refreshrate for datatransfer from the plugins to the service

    def __init__(self, service_handler: ServiceTalkerBase, data: Dict[str, Any],
                 *args: Any, **kwargs: Any) -> None:
        """
        init core data and call profile function defined in `data`

        Parameters
        ----------
        service_handler : object
            the connector to the (native) keyboard driver
        data : dict
            config data for this plugin (see also `preferences_struct()`)
        args : *args
            derived from threading.Thread
        kwargs : **kwargs
            derived from threading.Thread
        """
        super().__init__(*args, **kwargs)
        logging.debug("startup %s", type(self).__name__)
        if not data:
            logging.fatal("no datasection in the config file for '%s'!", type(self).__name__)
            raise ValueError("no datasection in the config file for '%s'!" % type(self).__name__)
        self._stop_event = threading.Event()
        self.service_handler = service_handler
        self.data = data

    def stop(self) -> None:
        """set flag to stop thread"""
        self._stop_event.set()

    def stopped(self) -> bool:
        """
        returns True if thread should be stopped

        Return
        ------
        bool
            True if thread should be ending
        """
        return self._stop_event.is_set()

    def run(self) -> None:
        """worker thread dummy function"""
        return

    @staticmethod
    def schedule_event() -> Dict:
        """event dummy function"""
        return {}


try:
    import wx

    FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
    BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
    FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)
    KB_DEFS_FOLDER = "keyboard_defs"


    class PluginsCoreGUI(wx.Panel):
        """base class for all plugin gui classes"""
        i_keytable = {}
        """all keys separated by vendor"""
        i_effecttable = {}
        """all effects separated by vendor"""

        # it seems that pylint counts the also the locals from the parent
        # pylint: disable=too-many-locals
        def __init__(self, parent: wx.Panel, wx_id: int = wx.ID_ANY,
                     pos: wx.Point = wx.DefaultPosition,
                     size: wx.Size = wx.Size(500, 300), style: int = wx.TAB_TRAVERSAL,
                     name: str = wx.EmptyString) -> None:
            """
            set up the instance tables for keys and effects separated by vendor.

            Parse the keyboard definition files th get all keys and effects.
            """
            super().__init__(parent, wx_id, pos, size, style, name)
            self.keytable = []
            self.effecttable = []

            if PluginsCoreGUI.i_keytable:
                return
            for filename in glob.glob1(os.path.join(BASEDIR_SCRIPT, KB_DEFS_FOLDER), "*.json"):
                filename = os.path.join(BASEDIR_SCRIPT, KB_DEFS_FOLDER, filename)
                with open(filename) as json_file:
                    data = json.load(json_file)
                    vendor = data["vendor"]
                    if vendor not in PluginsCoreGUI.i_effecttable:
                        PluginsCoreGUI.i_effecttable[vendor] = []
                    if vendor not in PluginsCoreGUI.i_keytable:
                        PluginsCoreGUI.i_keytable[vendor] = []
                    for key in data:
                        if key == "vendor":
                            continue
                        model = data[key]
                        key_list = PluginsCoreGUI.i_effecttable[vendor]
                        element_list = model["actions"]
                        for element in element_list:
                            if element not in key_list:
                                key_list.append(element)
                        key_list = PluginsCoreGUI.i_keytable[vendor]
                        element_list = model["names"]
                        for element in element_list:
                            if element not in key_list:
                                key_list.append(element)

        def set_data(self, data: Dict[str, Any], vendor: str) -> None:
            """
            set values of the controls

            Parameters
            ----------
            data : dict
                data to set controls; see also `preferences_struct()`
            vendor : str
                to set vendor specific menu entries
            """
            raise NotImplementedError

        def get_data(self) -> Dict[str, Any]:
            """
            get values from the controls

            Return
            ------
            dict containing the control values
            """
            raise NotImplementedError

        def add_new_row(self, event: wx.Event) -> None:
            """
            event handler to add a new row

            Parameters
            ----------
            event : wx.event
                event object
            """
            raise NotImplementedError

        def remove_row(self, event: wx.Event) -> None:
            """
            event handler to remove a new row

            Parameters
            ----------
            event : wx.event
                event object
            """
            raise NotImplementedError

        def set_value(self, widget: wx.Window, value: Union[str, wx.Colour, None]) -> None:
            """set value of a control (helper function)"""
            if isinstance(widget, wx.ColourPickerCtrl):
                self.set_color(widget, value)
            elif isinstance(widget, ExColourPickerCtrl):
                self.set_color(widget, value)
            elif isinstance(widget, wx.Choice):
                self.set_menustring(widget, value)
            elif isinstance(widget, wx.RadioBox):
                self.set_menustring(widget, value)
            elif isinstance(widget, wx.ComboBox):
                widget.SetValue(value)
            elif isinstance(widget, wx.TextCtrl):
                widget.SetValue(value)
            elif isinstance(widget, wx.Slider):
                widget.SetValue(int(value))
            else:
                widget.SetLabel(value)

        def get_value(self, widget: wx.Window) -> str:
            """get value of a control (helper function)"""
            if isinstance(widget, wx.ColourPickerCtrl):
                ret_val = self.get_color(widget)
            elif isinstance(widget, ExColourPickerCtrl):
                ret_val = self.get_color(widget)
            elif isinstance(widget, wx.Choice):
                ret_val = self.get_menustring(widget)
            elif isinstance(widget, wx.RadioBox):
                ret_val = self.get_menustring(widget)
            elif isinstance(widget, wx.ComboBox):
                ret_val = widget.GetValue()
            elif isinstance(widget, wx.TextCtrl):
                ret_val = widget.GetValue()
            elif isinstance(widget, wx.Slider):
                ret_val = str(widget.GetValue())
            else:
                ret_val = widget.GetLabel()
            return ret_val

        @staticmethod
        def set_color(picker: Union[wx.ColourPickerCtrl, ExColourPickerCtrl],
                      color: Optional[wx.Colour]) -> None:
            """set color of a color picker from raw hex string"""
            if color:
                picker.SetColour("#%s" % color)
            else:
                picker.SetColour(None)

        @staticmethod
        def set_menustring(menu: Union[wx.Choice, wx.RadioBox], name: str) -> None:
            """set menu selection by string (name)"""
            idx = menu.FindString(name)
            if idx == wx.NOT_FOUND:
                idx = 0
            menu.SetSelection(idx)

        @staticmethod
        def get_color(picker: Union[wx.ColourPickerCtrl, ExColourPickerCtrl]) -> str:
            """get color of a color picker as raw hex string"""
            color = picker.GetColour()
            if color:
                return color.GetAsString(wx.C2S_HTML_SYNTAX)[1:]
            return ""

        @staticmethod
        def get_menustring(menu: Union[wx.Choice, wx.RadioBox]) -> str:
            """get selected menu string (name)"""
            return menu.GetString(menu.GetSelection())

        @staticmethod
        def change_menucontent(menu: Union[wx.Choice, wx.RadioBox], new_list: List[str]) -> None:
            """replace the content of a menu"""
            menu.Clear()
            menu.AppendItems(new_list)

        def check_key_input(self, event: wx.CommandEvent) -> None:
            """check text in control if it is a correct value for a key"""
            event.Skip()
            obj = event.GetEventObject()
            selection = obj.GetSelection()
            value = obj.GetValue()
            obj.ChangeValue(value)
            obj.SetSelection(*selection)
            current_text = event.GetString()
            for choice in self.keytable:
                if choice.startswith(current_text):
                    obj.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
                    return
            obj.SetBackgroundColour(wx.Colour(255, 0, 0))

except ImportError:
    print("wx framework not found.")
    wx = None
