# -*- coding: utf-8 -*-

###########################################################################
# Python code generated with wxFormBuilder (version 3.9.0 Jan 27 2020)
# http://www.wxformbuilder.org/
#
# PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

from typing import Optional


###########################################################################
# Class ExColourPickerCtrlBase
###########################################################################

class ExColourPickerCtrlBase(wx.Panel):

    def __init__(self, parent, wx_id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(80, 40),
                 style=wx.BORDER_RAISED | wx.BORDER_SUNKEN | wx.TAB_TRAVERSAL, name=wx.EmptyString):
        wx.Panel.__init__(self, parent, id=wx_id, pos=pos, size=size, style=style, name=name)

        b_sizer = wx.BoxSizer()

        self.m_check_enable = wx.CheckBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_check_enable.SetValue(True)
        b_sizer.Add(self.m_check_enable, 0, wx.ALL | wx.EXPAND, 5)

        self.m_colour_picker = wx.ColourPickerCtrl(self, wx.ID_ANY, wx.BLACK, wx.DefaultPosition, wx.DefaultSize)
        self.m_colour_picker.Enable(False)
        self.m_colour_picker.SetMaxSize(wx.Size(80, -1))

        b_sizer.Add(self.m_colour_picker, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 0)

        self.SetSizer(b_sizer)
        self.Layout()

        # Connect Events
        self.m_check_enable.Bind(wx.EVT_CHECKBOX, self.on_checkbox)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def on_checkbox(self, event):
        event.Skip()

###########################################################################
# Python code generated from the wxFormBuilder end
###########################################################################


###########################################################################
# Class ExColourPickerCtrl
###########################################################################


class ExColourPickerCtrl(ExColourPickerCtrlBase):

    def __init__(self, parent: wx.Window, wx_id: int = wx.ID_ANY, color: Optional[wx.Colour] = None) -> None:
        super(ExColourPickerCtrl, self).__init__(parent, wx_id)

        if color:
            self.m_colour_picker.SetColour(color)
            self.m_check_enable.SetValue(True)
            self.m_colour_picker.Enable(False)

    # noinspection PyPep8Naming
    def SetColour(self, color: Optional[wx.Colour]) -> None:
        if color:
            self.m_check_enable.SetValue(True)
            self.m_colour_picker.Enable()
            self.m_colour_picker.SetColour(color)
        else:
            self.m_check_enable.SetValue(False)
            self.m_colour_picker.Disable()

    # noinspection PyPep8Naming
    def GetColour(self) -> Optional[wx.Colour]:
        if self.m_check_enable.GetValue():
            return self.m_colour_picker.GetColour()
        return None

    # Virtual event handlers, overide them in your derived class
    def on_checkbox(self, event: wx.Event) -> None:
        if self.m_check_enable.GetValue():
            self.m_colour_picker.Enable()
        else:
            self.m_colour_picker.Disable()

        event.Skip()
