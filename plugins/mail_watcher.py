#!/usr/bin/env python3
# coding=utf-8
"""
this module monitors mailboxes
"""

import imaplib
import logging
import os
import time
import threading

from typing import Any, Dict, List

import cryptography.fernet as fernet

from common.plugins_core import PluginsCore
from common.ExColourPickerCtrl import ExColourPickerCtrl
from common.core_data import CONFIG_PATH
from services.service_talker import ServiceTalkerBase, LedAttrs


def preferences_struct() -> Dict:
    """
    json preferences structure for mail_watcher

    * host: mail host
    * username: mailbox username
    * password: mailbox password (not encrypted!)
    * effect: alert effect
    * color: alert color
    * bg_effect:
    * bg_color: second alert effect
    * boxes: dict of mailboxes that should checked for unseen emails
        * key: IMAP name of the mail folder
        * value: list of keys that should be used for alerting

    Return
    ------
    structure of the config file entry
    """
    return {
        "host": "mail.company.invalid",
        "username": "user@company.invalid",
        "password": "",
        "effect": "Flashing",
        "color": "FF0000",
        "bg_effect": "Direct",
        "bg_color": "00FF00",
        "boxes": {
            "INBOX": [
                "Key: Insert",
                "Key: Home",
                "Key: Page Up"
            ],
            "INBOX.Google": [
                "Key: Delete",
                "Key: End",
                "Key: Page Down"
            ]
        }
    }


DB_FILE_PATH = os.path.join(CONFIG_PATH, 'keyboard_led.db')


class MailWatcher(PluginsCore):
    """
        This plugin looks for unseen emails on a mailserver
    """

    def __init__(self, service_handler: ServiceTalkerBase, data: Dict[str, Any],
                 *args: Any, **kwargs: Any) -> None:
        """
        init core data

        Parameters
        ----------
        service_handler : object
            the connector to the (native) keyboard driver
        data : dict
            config data for this plugin (see also `preferences_struct()`)
        args : *args
            derived from threading.Thread
        kwargs : **kwargs
            derived from threading.Thread
        """
        super().__init__(service_handler, data, *args, **kwargs)
        try:
            file = open(DB_FILE_PATH, mode='rb')
        except IOError as io_ex:
            raise RuntimeError("no valid key_db found!") from io_ex

        key = file.read()
        cipher_suite = fernet.Fernet(key)
        file.close()
        self.mail_thread_started = False
        self.mail_thread = None
        self.results = {}
        self.old_state = {}
        for key in self.data:
            if key.endswith('effect'):
                self.data[key] = service_handler.normalize_action(self.data[key])
            if key.endswith('color'):
                self.data[key] = self.data[key].lower()
        if 'bg_effect' not in data:
            data['bg_effect'] = None
        if 'bg_color' not in data:
            data['bg_color'] = None
        for box in self.data['boxes']:
            self.old_state[box] = False
            new_list = []
            for keycodes in self.data['boxes'][box]:
                new_list.append(service_handler.normalize_keycode(keycodes))
            self.data['boxes'][box] = new_list
        password = data['password']
        password = password.encode()
        try:
            self.password = cipher_suite.decrypt(password).decode()
        except fernet.InvalidToken as inval_token:
            raise RuntimeError("Invalid mailserver password!") from inval_token

    def check_mail(self) -> None:
        """
        iterate over multiple boxes and check for unseen emails
        """
        self.results = {}
        conn = IMAP(self.data['host'], self.data['username'], self.password)
        for box in self.data['boxes']:
            self.results[box] = conn.check_for_new_mail(box)
        del conn

    def stop(self) -> None:
        if self.mail_thread and self.mail_thread.is_alive():
            self.mail_thread.join()

    def schedule_event(self) -> Dict[str, List]:
        """
        start imap check or look for a result of an already started check

        to avoid waitings for the imap server we start a background task for imap checking;
        if it is already startet, we check if it still running or if it is ready.

        Return
        ------
        list
            a list of changed check results if the task is ready. If the states haven't changed
            according to the last check results then the list is empty.
            On thread startup and on non finished thread run the list is always empty.
        """
        ret_val = {}
        if self.mail_thread_started:
            if self.mail_thread.is_alive():
                logging.debug("thread still running")
            else:
                logging.debug("thread is ready")
                self.mail_thread = None
                self.mail_thread_started = False
                for box, state in self.results.items():
                    if self.old_state[box] != state:
                        if state:
                            template = [LedAttrs(self.data['effect'], self.data['color'],
                                                self.data['bg_effect'], self.data['bg_color'])]
                        else:
                            template = []
                        self.old_state[box] = state
                        for key in self.data['boxes'][box]:
                            ret_val[key] = template
        else:
            logging.debug("start thread")
            self.mail_thread_started = True
            self.mail_thread = threading.Thread(target=self.check_mail, args=())
            self.mail_thread.start()
        return ret_val

    # for standalone-only run task
    # pylint: disable=broad-except
    def run_task(self) -> None:
        """helper function for standalone(test) calls"""
        while True:
            try:
                logging.debug("result: '%s'", self.schedule_event())
                time.sleep(15)
            except Exception as ex:
                print("error occured! (%s)" % ex)
                time.sleep(60)


try:
    import wx

    # pylint: disable=ungrouped-imports  # allow to run without wx installed
    from common.plugins_core import PluginsCoreGUI

    # this is necessary for wx, sorry
    # pylint: disable=no-member,too-many-arguments,line-too-long
    # pylint: disable=too-many-instance-attributes,too-many-statements
    class MailWatcherGUI(PluginsCoreGUI):
        """
        gui elements for mailwatcher
        """
        def __init__(self, parent: wx.Panel) -> None:
            """
            create controls to setup this plugin

            Parameters
            ----------
            parent : wx.widgets
                parent windows object to insert new gui elements
            """
            super().__init__(parent)
            b_sizer1 = wx.BoxSizer(wx.VERTICAL)

            self.m_pane = wx.ScrolledWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.HSCROLL | wx.VSCROLL)
            self.m_pane.SetScrollRate(5, 5)
            fg_sizer1 = wx.FlexGridSizer(0, 2, 0, 0)
            fg_sizer1.AddGrowableCol(1)
            fg_sizer1.SetFlexibleDirection(wx.BOTH)
            fg_sizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

            self.m_static_text1 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Host", wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_static_text1.Wrap(-1)

            fg_sizer1.Add(self.m_static_text1, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_text_host = wx.TextCtrl(self.m_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(300, -1),
                                           0)
            fg_sizer1.Add(self.m_text_host, 0, wx.ALL, 5)

            self.m_static_text2 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize,
                                                0)
            self.m_static_text2.Wrap(-1)

            fg_sizer1.Add(self.m_static_text2, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_text_username = wx.TextCtrl(self.m_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.Size(300, -1), 0)
            fg_sizer1.Add(self.m_text_username, 0, wx.ALL, 5)

            self.m_static_text3 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize,
                                                0)
            self.m_static_text3.Wrap(-1)

            fg_sizer1.Add(self.m_static_text3, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_text_password = wx.TextCtrl(self.m_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                               wx.Size(300, -1), wx.TE_PASSWORD)
            fg_sizer1.Add(self.m_text_password, 0, wx.ALL, 5)

            self.m_static_text5 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Effect", wx.DefaultPosition, wx.DefaultSize,
                                                0)
            self.m_static_text5.Wrap(-1)

            fg_sizer1.Add(self.m_static_text5, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_choice_effect = wx.Choice(self.m_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                             self.effecttable, 0)
            self.m_choice_effect.SetSelection(0)
            fg_sizer1.Add(self.m_choice_effect, 0, wx.ALL, 5)

            self.m_static_text6 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Color", wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_static_text6.Wrap(-1)

            fg_sizer1.Add(self.m_static_text6, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_colour_effect = wx.ColourPickerCtrl(self.m_pane, wx.ID_ANY, wx.BLACK, wx.DefaultPosition,
                                                       wx.DefaultSize)
            fg_sizer1.Add(self.m_colour_effect, 0, wx.ALL, 5)

            self.m_static_text7 = wx.StaticText(self.m_pane, wx.ID_ANY, u"BG Effect", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
            self.m_static_text7.Wrap(-1)

            fg_sizer1.Add(self.m_static_text7, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_choice_bg_effect = wx.Choice(self.m_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                                self.effecttable, 0)
            self.m_choice_bg_effect.SetSelection(0)
            fg_sizer1.Add(self.m_choice_bg_effect, 0, wx.ALL, 5)

            self.m_static_text8 = wx.StaticText(self.m_pane, wx.ID_ANY, u"BG Color", wx.DefaultPosition, wx.DefaultSize,
                                                0)
            self.m_static_text8.Wrap(-1)

            fg_sizer1.Add(self.m_static_text8, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_colour_bg_effect = ExColourPickerCtrl(self.m_pane)
            fg_sizer1.Add(self.m_colour_bg_effect, 0, wx.ALL, 5)

            self.m_static_text10 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Mailboxes", wx.DefaultPosition,
                                                 wx.DefaultSize, 0)
            self.m_static_text10.Wrap(-1)

            fg_sizer1.Add(self.m_static_text10, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_add_mailbox = wx.Button(self.m_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                           wx.BU_EXACTFIT)

            self.m_add_mailbox.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK, wx.ART_BUTTON))
            fg_sizer1.Add(self.m_add_mailbox, 0, wx.ALL, 5)

            fg_sizer1.Add((0, 0), 1, wx.EXPAND, 5)

            self.m_panel_mailbox = wx.Panel(self.m_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.TAB_TRAVERSAL)
            b_sizer5 = wx.BoxSizer(wx.VERTICAL)

            self.m_panel_mailbox.SetSizer(b_sizer5)
            self.m_panel_mailbox.Layout()
            b_sizer5.Fit(self.m_panel_mailbox)
            fg_sizer1.Add(self.m_panel_mailbox, 1, wx.ALL | wx.EXPAND, 5)

            self.m_pane.SetSizer(fg_sizer1)
            self.m_pane.Layout()
            fg_sizer1.Fit(self.m_pane)
            b_sizer1.Add(self.m_pane, 1, wx.EXPAND | wx.ALL, 5)

            self.SetSizer(b_sizer1)
            self.Layout()

            # Connect Events
            self.m_add_mailbox.Bind(wx.EVT_LEFT_UP, self.add_new_row)

            try:
                with open(DB_FILE_PATH, "rb") as file:
                    self.key = file.read()
            except (IOError, TypeError):
                self.key = fernet.Fernet.generate_key()
                with open(DB_FILE_PATH, "wb+") as file:
                    file.write(self.key)
            self.saved_password = ""

        def add_row(self, parent: wx.Panel, data: List, name: str) -> None:
            """
            add a row for variable settings

            Parameters
            ----------
            parent : wx.window
                parent windows object to insert new row
            data : list
                data for the controls
            name : str
                name of the row object if not an enumeration
            """
            m_choices = []
            m_mailbox_panel = wx.Panel(parent, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            b_sizer11 = wx.BoxSizer()

            m_text_box_name = wx.TextCtrl(m_mailbox_panel, wx.ID_ANY, name, wx.DefaultPosition, wx.Size(140, -1), 0)
            b_sizer11.Add(m_text_box_name, 1, wx.EXPAND | wx.ALL, 5)

            for idx in range(3):
                txt_ctrl = wx.TextCtrl(m_mailbox_panel, wx.ID_ANY, "", wx.DefaultPosition, wx.Size(140, -1))
                txt_ctrl.AutoComplete(self.keytable)
                b_sizer11.Add(txt_ctrl, 0, wx.ALL, 5)
                txt_ctrl.Bind(wx.EVT_TEXT, self.check_key_input)
                m_choices.append(txt_ctrl)

            m_button_remove = wx.Button(m_mailbox_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.BU_EXACTFIT)
            m_button_remove.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_DEL_BOOKMARK, wx.ART_BUTTON))
            b_sizer11.Add(m_button_remove, 0, wx.ALL, 5)

            m_mailbox_panel.SetSizer(b_sizer11)
            m_mailbox_panel.Layout()
            b_sizer11.Fit(m_mailbox_panel)

            sizer = parent.GetSizer()
            sizer.Add(m_mailbox_panel, 0, wx.ALL, 5)

            self.Layout()

            m_button_remove.Bind(wx.EVT_LEFT_UP, self.remove_row)

            for idx, choice_key in enumerate(m_choices):
                if idx < len(data):
                    self.set_value(choice_key, data[idx])

        def set_data(self, data: Dict[str, Any], vendor: str) -> None:
            """
            set values of the controls

            Parameters
            ----------
            data : dict
                data to set controls; see also `preferences_struct()`
            vendor : str
                to set vendor specific menu entries
            """
            self.keytable = PluginsCoreGUI.i_keytable[vendor]
            self.effecttable = PluginsCoreGUI.i_effecttable[vendor]
            self.change_menucontent(self.m_choice_effect, self.effecttable)
            self.change_menucontent(self.m_choice_bg_effect, self.effecttable)
            self.set_value(self.m_text_host, data["host"])
            self.set_value(self.m_text_username, data["username"])
            self.set_value(self.m_choice_effect, data["effect"])
            self.set_value(self.m_colour_effect, data["color"])
            self.set_value(self.m_choice_bg_effect, data["bg_effect"])
            self.set_value(self.m_colour_bg_effect, data["bg_color"])
            for child in self.m_panel_mailbox.GetChildren():
                child.Destroy()

            for name, data2 in data["boxes"].items():
                self.add_row(self.m_panel_mailbox, data2, name)
            self.Fit()

            self.saved_password = data["password"]

        def get_data(self) -> Dict[str, Any]:
            """
            get values from the controls

            Return
            ------
            dict containing the control values
            """
            data = {
                "boxes": {},
                "host": self.get_value(self.m_text_host),
                "username": self.get_value(self.m_text_username),
                "effect": self.get_value(self.m_choice_effect),
                "color": self.get_value(self.m_colour_effect),
                "bg_effect": self.get_value(self.m_choice_bg_effect),
                "bg_color": self.get_value(self.m_colour_bg_effect)
            }
            password = self.get_value(self.m_text_password)
            if password:
                cipher_suite = fernet.Fernet(self.key)
                password = cipher_suite.encrypt(password.encode())
                data["password"] = password.decode()
            else:
                data["password"] = self.saved_password

            for child in self.m_panel_mailbox.GetChildren():
                row_data = []
                for elem in child.GetChildren():
                    if not isinstance(elem, wx.TextCtrl):
                        continue
                    dat = self.get_value(elem)
                    if dat:
                        row_data.append(dat)
                if row_data:
                    idx = row_data.pop(0)
                    data["boxes"][idx] = row_data
            return data

        def add_new_row(self, event: wx.Event) -> None:
            """
            event handler to add a new row

            Parameters
            ----------
            event : wx.event
                event object
            """
            parent = event.GetEventObject()
            parent = parent.GetNextSibling()
            self.add_row(parent, ["", "000000", "ffffff"], "box_name")
            self.Layout()

        def remove_row(self, event: wx.Event) -> None:
            parent = event.GetEventObject()
            parent = parent.GetParent()
            parent.Destroy()
            self.Layout()

except ImportError:
    print("wx framework not found.")
    wx = None
    PluginsCoreGUI = None


class IMAP():
    """
    small helper class to handle imap communication
    """

    def __init__(self, host: str, user: str, passwd: str) -> None:
        if not host or not user or not passwd:
            raise ValueError("parameter must not be empty!")
        self.connection = imaplib.IMAP4_SSL(host)
        self.connection.login(user, passwd)

    def check_for_new_mail(self, box: str) -> bool:
        """
        check if there are unseen emails in the named box

        Parameters
        ----------
        box : str
            IMAP name of the folder that should be checkd e.g. 'INBOX' or 'INBOX.subfolder'

        Return
        ------
        bool
            True if there is/are unseen email(s); False otherwise
        """
        self.connection.select(box)
        # search and return uids instead
        _result, data = self.connection.uid('search', "", "(UNSEEN)")
        if data[0]:
            return True
        return False

    def __del__(self) -> None:
        try:
            self.connection.shutdown()
        except AttributeError:
            pass
