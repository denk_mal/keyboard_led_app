#!/usr/bin/env python3
# coding=utf-8
"""
this module creates color profiles
"""

import math
import time
from typing import Any, Dict, List, Optional

from common.plugins_core import PluginsCore
from services.service_talker import ServiceTalkerBase, LedAttrs


def preferences_struct() -> Dict[str, Any]:
    """
    json preferences structure for profile

    * function: the function that should be activated on startup
    * parameters: parameters for the function call (see functions for
        further details)

    Return
    ------
    structure of the config file entry
    """
    return {
        "function": "unique",
        "color": "ffffff",
        "mode": "Direct",
        "offset": "0",
        "spped": "3",
        "direction": "Left"
    }


class Profile(PluginsCore):
    """
        This plugin set a profile for the keyboard on startup
    """

    def __init__(self, service_handler: ServiceTalkerBase, data: Dict[str, Any],
                 *args: Any, **kwargs: Any) -> None:
        """
        init core data and call profile function defined in `data`

        Parameters
        ----------
        service_handler : object
            the connector to the (native) keyboard driver
        data : dict
            config data for this plugin (see also `preferences_struct()`)
        args : *args
            derived from threading.Thread
        kwargs : **kwargs
            derived from threading.Thread
        """
        super().__init__(service_handler, data, *args, **kwargs)

        # build rainbow table
        self.rb_table = []
        for idx in range(0, 240):
            self.rb_table.append(int(round((((math.sin(math.radians((idx * 360 / 240) - 90))) \
                + 1) * 255) / 2)))
        for idx in range(240, 361):
            self.rb_table.append(0)
        self.offset_inc = 0

        # build keycode matrix for faster access
        dim = self.service_handler.get_dimensions()
        self.width = int(dim["width"])
        height = int(dim["height"])
        self.keycode_matrix: List[List[Optional[int]]] = \
            [[None] * self.width for _i in range(height)]
        for x_pos in range(self.width):
            for y_pos in range(height):
                self.keycode_matrix[y_pos][x_pos] = self.service_handler.normalize_keycode("%d,%d"
                        % (x_pos, y_pos))
        speed = int(data['speed'])
        if data['direction'] == 'Left':
            self.offset_inc = speed
        else:
            self.offset_inc = speed * -1
        self.set_profile()

    def run(self) -> None:
        """
        worker thread for rainbow flow

        writes the content of the rainbow table every 100 mSec to the profile
        """
        offset = 0
        while not self.stopped():
            self._rainbow(1, offset)
            time.sleep(PluginsCore.i_time_slice)
            offset = (offset + self.offset_inc) % 360

    def set_profile(self) -> None:
        """
        call dispatcher

        get method name and parameter from the config data and execute it
        """
        funct_name = "funct_%s" % self.data["function"]
        method_to_call = getattr(self, funct_name)
        method_to_call(self.data["color"], self.data["mode"], self.data["offset"])

    def funct_unique(self, color: str, mode: str, _offset: str) -> None:
        """
        profile function: set single color

        Parameters
        ----------
        color : str
            color of the keys
        mode : str
            effect of the keys
        _offset : str
            offset to the rainbow table for the start color (not used)
        """
        cmd = self.service_handler.normalize_action(mode)
        self.service_handler.set_profile(cmd, color)
        self.service_handler.end_transfer()

    def funct_rainbow(self, _color: str, mode: str, offset: str) -> None:
        """
        profile function: set static rainbow

        Parameters
        ----------
        _color : str
            color of the keys (not used)
        mode : str
            effect of the keys
        offset : str
            offset to the rainbow table for the start color
        """
        cmd = self.service_handler.normalize_action(mode)
        self._rainbow(cmd, int(offset))

    def funct_rainbow_flow(self, _color: str, _mode: str, _offset: str) -> None:
        """
        profile function: let the rainbow flow over the keyboard

        Parameters
        ----------
        _color : str
            color of the keys (not used)
        _mode : str
            effect of the keys (not used)
        _offset : str
            offset to the rainbow table for the start color (not used)
        """
        self.start()

    def _rainbow(self, effect: int, offset: int) -> None:
        """
        create a rainbow effect on the keyboard

        Parameters
        ----------
        effect : int
            key effect
        offset : int
            offset to the rainbow table for the start color
        """
        step = 360 / self.width
        for x_pos in range(0, self.width):
            angle = x_pos * step
            red = self.rb_table[int((angle + 120 + offset) % 360)]
            green = self.rb_table[int(angle + offset) % 360]
            blue = self.rb_table[int((angle + 240 + offset) % 360)]
            color = "%s%s%s" % (format(red, '02x'), format(green, '02x'), format(blue, '02x'))
            attribute = LedAttrs(effect, color)
            for keycode_row in self.keycode_matrix:
                key = keycode_row[x_pos]
                if key is not None:
                    self.service_handler.set_key(key, attribute, is_profile=True)
            self.service_handler.set_underglow_column(x_pos, attribute, is_profile=True)
        self.service_handler.end_transfer()


try:
    import wx

    # pylint: disable=ungrouped-imports  # allow to run without wx installed
    from common.plugins_core import PluginsCoreGUI

    # this is necessary for wx, sorry
    # pylint: disable=no-member,too-many-arguments,line-too-long
    # pylint: disable=too-many-instance-attributes,too-many-statements
    class ProfileGUI(PluginsCoreGUI):
        """
         unique(color, mode)
         rainbow(step_offset, mode)
         rainbow_flow()

         Parameters
         ----------
         parent : wx.window
             parent windows object to insert new gui elements
         """

        def __init__(self, parent: wx.Panel) -> None:
            """
            create controls to setup this plugin

            Parameters
            ----------
            parent : wx.window
                parent windows object to insert new gui elements
            """
            super().__init__(parent)

            m_function_choices = []
            for funct in dir(Profile):
                if callable(getattr(Profile, funct)):
                    if funct.startswith("funct_"):
                        m_function_choices.append(funct[len("funct_"):])
            m_param_mode_choices = [""]

            b_sizer1 = wx.BoxSizer(wx.VERTICAL)

            self.m_pane = wx.ScrolledWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.HSCROLL | wx.VSCROLL)
            self.m_pane.SetScrollRate(5, 5)
            b_sizer2 = wx.BoxSizer(wx.VERTICAL)

            self.m_function = wx.RadioBox(self.m_pane, wx.ID_ANY, u"Profile", wx.DefaultPosition, wx.DefaultSize,
                                          m_function_choices, 1, wx.RA_SPECIFY_COLS)
            self.m_function.SetSelection(0)
            b_sizer2.Add(self.m_function, 0, wx.ALL | wx.EXPAND, 5)

            self.m_param_color = wx.ColourPickerCtrl(self.m_pane, wx.ID_ANY, wx.Colour(0, 0, 0), wx.DefaultPosition,
                                                     wx.DefaultSize, 0)
            b_sizer2.Add(self.m_param_color, 0, wx.ALL, 5)

            self.m_param_mode = wx.Choice(self.m_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                          m_param_mode_choices, 0)
            self.m_param_mode.SetSelection(0)
            b_sizer2.Add(self.m_param_mode, 0, wx.ALL | wx.EXPAND, 5)

            self.m_static_text1 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Offset", wx.DefaultPosition, wx.DefaultSize, 0)
            self.m_static_text1.Wrap(-1)

            b_sizer2.Add(self.m_static_text1, 0, wx.ALL, 5)

            self.m_param_offset = wx.Slider(self.m_pane, wx.ID_ANY, 0, 0, 360, wx.DefaultPosition, wx.DefaultSize,
                                            wx.SL_HORIZONTAL | wx.SL_LABELS)
            b_sizer2.Add(self.m_param_offset, 0, wx.ALL | wx.EXPAND, 5)

            sb_sizer3 = wx.StaticBoxSizer(wx.StaticBox(self.m_pane, wx.ID_ANY, u"Speed and Direction"), wx.HORIZONTAL)

            self.m_param_speed = wx.Slider(sb_sizer3.GetStaticBox(), wx.ID_ANY, 3, 1, 8, wx.DefaultPosition,
                                           wx.DefaultSize, wx.SL_HORIZONTAL | wx.SL_LABELS)
            sb_sizer3.Add(self.m_param_speed, 1, wx.ALL, 5)

            m_param_direction_choices = [u"Left", u"Right"]
            self.m_param_direction = wx.RadioBox(sb_sizer3.GetStaticBox(), wx.ID_ANY, u"Direction", wx.DefaultPosition,
                                                 wx.DefaultSize, m_param_direction_choices, 1, wx.RA_SPECIFY_COLS)
            self.m_param_direction.SetSelection(0)
            sb_sizer3.Add(self.m_param_direction, 1, wx.ALL, 5)

            b_sizer2.Add(sb_sizer3, 0, wx.EXPAND, 5)

            self.m_pane.SetSizer(b_sizer2)
            self.m_pane.Layout()
            b_sizer2.Fit(self.m_pane)
            b_sizer1.Add(self.m_pane, 1, wx.EXPAND | wx.ALL, 5)

            self.SetSizer(b_sizer1)
            self.Layout()

            # Connect Events
            self.m_function.Bind(wx.EVT_RADIOBOX, self.select_profile)

        def set_data(self, data: Dict[str, Any], vendor: str) -> None:
            """
            set values of the controls

            Parameters
            ----------
            data : dict
                data to set controls; see also `preferences_struct()`
            vendor : str
                to set vendor specific menu entries
            """
            effecttable = PluginsCoreGUI.i_effecttable[vendor]
            try:
                self.set_value(self.m_function, data['function'])
                self.set_value(self.m_param_color, data['color'])
                self.set_value(self.m_param_offset, data['offset'])
                self.change_menucontent(self.m_param_mode, effecttable)
                self.set_value(self.m_param_mode, data['mode'])
                self.set_value(self.m_param_speed, data['speed'])
                self.set_value(self.m_param_direction, data['direction'])
            except KeyError:
                pass
            self.select_profile(None)

        def get_data(self) -> Dict[str, Any]:
            """
            get values from the controls

            Return
            ------
            dict containing the control values
            """
            data = {
                'function': self.get_value(self.m_function),
                'color': self.get_value(self.m_param_color),
                'mode': self.get_value(self.m_param_mode),
                'offset': self.get_value(self.m_param_offset),
                'speed': self.get_value(self.m_param_speed),
                'direction': self.get_value(self.m_param_direction)
            }
            return data

        def add_new_row(self, event: wx.Event) -> None:
            pass

        def remove_row(self, event: wx.Event) -> None:
            pass

        # Virtual event handlers, overide them in your derived class
        def select_profile(self, _event: Optional[wx.Event]):
            """
            handle selection of a profile
            """
            obj = self.m_function  # event.GetEventObject()
            self.m_param_color.Disable()
            self.m_param_mode.Disable()
            self.m_param_offset.Disable()
            self.m_param_speed.Disable()
            self.m_param_direction.Disable()
            profile = self.get_value(obj)
            if profile == 'unique':
                self.m_param_color.Enable()
                self.m_param_mode.Enable()
            elif profile == 'rainbow':
                self.m_param_offset.Enable()
            elif profile == 'rainbow_flow':
                self.m_param_speed.Enable()
                self.m_param_direction.Enable()


except ImportError:
    print("wx framework not found.")
    wx = None
    PluginsCoreGUI = None
