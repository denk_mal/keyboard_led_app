#!/usr/bin/env python3
# coding=utf-8
"""
this module monitors some system parameters
"""

import logging
import time
from typing import Any, Dict, List

import psutil

from common.plugins_core import PluginsCore
from common.ExColourPickerCtrl import ExColourPickerCtrl
from services.service_talker import ServiceTalkerBase, LedAttrs


def preferences_struct() -> Dict[str, Any]:
    """
    json preferences structure for system_view

    * cpu_load: a dict containing the levels for the cpu gauge  <br/>
        for every level there is a list containing key code, active color
        and (optional) passive color
    * lock_keys: (not implemented yet) <br/>
        for every lock there is a list containing key code, active color
        and (optional) passive color

    Return
    ------
    structure of the config file entry
    """
    return {
        "cpu_load": {
            "0": ["Key: 1", "00FF00", "FFFFFF"],
            "1": ["Key: 2", "00FF00", "FFFFFF"],
            "2": ["Key: 3", "00FF00", "FFFFFF"],
            "3": ["Key: 4", "00FF00", "FFFFFF"],
            "4": ["Key: 5", "FFFF00", "FFFFFF"],
            "5": ["Key: 6", "FFFF00", "FFFFFF"],
            "6": ["Key: 7", "FFA500", "FFFFFF"],
            "7": ["Key: 8", "FF0000", "FFFFFF"],
            "8": ["Key: 9", "FF0000", "FFFFFF"],
            "9": ["Key: 0", "FF0000", "FFFFFF"]
        },
        "lock_keys": {
            "num_lock": ["Key: Num Lock", "0000FF"]
        }
    }


class SystemView(PluginsCore):
    """
        This plugin set the key color depending on system values
    """

    def __init__(self, service_handler: ServiceTalkerBase, data: Dict[str, Any],
                 *args: Any, **kwargs: Any) -> None:
        """
        init core data and start thread

        Parameters
        ----------
        service_handler : object
            the connector to the (native) keyboard driver
        data : dict
            config data for this plugin (see also `preferences_struct()`)
        args : *args
            derived from threading.Thread
        kwargs : **kwargs
            derived from threading.Thread
        """
        super().__init__(service_handler, data, *args, **kwargs)

        self.cpu_load_keys = {}
        for key in data['cpu_load']:
            key_data = data['cpu_load'][key]
            key_code = service_handler.normalize_keycode(key_data[0])
            if key_code == "00":
                logging.fatal("incorrect keycode '%s' in config file for '%s'!",
                              key_data[0], type(self).__name__)
                raise ValueError("incorrect keycode '%s' in config file for '%s'!" %
                                 (key_data[0], type(self).__name__))
            key_data[0] = key_code
            self.cpu_load_keys[int(key)] = key_data
        self.cpu_load_key_step = 100 // len(self.cpu_load_keys)
        self.start()

    def run(self) -> None:
        """
        worker thread for the system diplay

        sets the system values to the keyboard
        """
        while not self.stopped():
            self.cpu_load()
            self.keylock_state()
            time.sleep(PluginsCore.i_time_slice)

    def cpu_load(self) -> None:
        """
        get the cpu load and display a bar
        """
        # perc1 = psutil.cpu_percent()
        # if perc1 > 10:
        #     x = 1
        percent = int(psutil.cpu_percent() // self.cpu_load_key_step)
        for idx in range(len(self.cpu_load_keys)):
            key = self.cpu_load_keys[idx][0]
            if percent > idx:
                color = self.cpu_load_keys[idx][1]
            else:
                color = self.cpu_load_keys[idx][2]
            self.service_handler.set_key(key, LedAttrs(1, color))

    # pylint: disable=no-self-use
    def keylock_state(self) -> None:
        """
        show state of the lock key onto the key itselfs (not implemented yet)
        """
        return


try:
    import wx

    # pylint: disable=ungrouped-imports  # allow to run without wx installed
    from common.plugins_core import PluginsCoreGUI

    # this is necessary for wx, sorry
    # pylint: disable=no-member,too-many-arguments,line-too-long
    # pylint: disable=too-many-instance-attributes,too-many-statements
    class SystemViewGUI(PluginsCoreGUI):
        """
         Parameters
         ----------
         parent : wx.window
             parent windows object to insert new gui elements
         """
        i_lock_choices = ["num_lock", "caps_lock", "scroll_lock"]

        def __init__(self, parent: wx.Panel) -> None:
            """
            create controls to setup this plugin

            Parameters
            ----------
            parent : wx.widgets
                parent windows object to insert new gui elements
            """
            super().__init__(parent)
            b_sizer1 = wx.BoxSizer(wx.VERTICAL)

            self.m_pane = wx.ScrolledWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                            wx.HSCROLL | wx.VSCROLL)
            self.m_pane.SetScrollRate(5, 5)
            fg_sizer1 = wx.FlexGridSizer(0, 2, 0, 0)
            fg_sizer1.AddGrowableCol(1)
            fg_sizer1.SetFlexibleDirection(wx.BOTH)
            fg_sizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

            self.m_static_text1 = wx.StaticText(self.m_pane, wx.ID_ANY, u"CPU Load", wx.DefaultPosition, wx.DefaultSize,
                                                0)
            self.m_static_text1.Wrap(-1)

            fg_sizer1.Add(self.m_static_text1, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_add_cpu = wx.Button(self.m_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                       wx.BU_EXACTFIT, wx.DefaultValidator, u"add_cpu")
            self.m_add_cpu.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK, wx.ART_BUTTON))
            fg_sizer1.Add(self.m_add_cpu, 0, wx.ALL, 5)

            fg_sizer1.Add((0, 0), 1, wx.EXPAND, 5)

            self.m_panel_cpu = wx.Panel(self.m_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            b_sizer5 = wx.BoxSizer(wx.VERTICAL)

            self.m_panel_cpu.SetSizer(b_sizer5)
            self.m_panel_cpu.Layout()
            b_sizer5.Fit(self.m_panel_cpu)
            fg_sizer1.Add(self.m_panel_cpu, 1, wx.ALL | wx.EXPAND, 5)

            self.m_static_text2 = wx.StaticText(self.m_pane, wx.ID_ANY, u"Lock Keys", wx.DefaultPosition,
                                                wx.DefaultSize, 0)
            self.m_static_text2.Wrap(-1)

            fg_sizer1.Add(self.m_static_text2, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

            self.m_add_lock = wx.Button(self.m_pane, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.BU_EXACTFIT, wx.DefaultValidator, u"add_lock")
            self.m_add_lock.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_ADD_BOOKMARK, wx.ART_BUTTON))
            fg_sizer1.Add(self.m_add_lock, 0, wx.ALL, 5)

            fg_sizer1.Add((0, 0), 1, wx.EXPAND, 5)

            self.m_panel_lock = wx.Panel(self.m_pane, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            b_sizer6 = wx.BoxSizer(wx.VERTICAL)

            self.m_panel_lock.SetSizer(b_sizer6)
            self.m_panel_lock.Layout()
            b_sizer6.Fit(self.m_panel_lock)
            fg_sizer1.Add(self.m_panel_lock, 0, wx.EXPAND | wx.ALL, 5)

            self.m_pane.SetSizer(fg_sizer1)
            self.m_pane.Layout()
            fg_sizer1.Fit(self.m_pane)
            b_sizer1.Add(self.m_pane, 1, wx.EXPAND | wx.ALL, 5)

            self.SetSizer(b_sizer1)
            self.Layout()

            # Connect Events
            self.m_add_cpu.Bind(wx.EVT_LEFT_UP, self.add_new_row)
            self.m_add_lock.Bind(wx.EVT_LEFT_UP, self.add_new_row)

        def add_row(self, parent: wx.Panel, data: List[str], name: str) -> None:
            """
            add a row for variable settings

            Parameters
            ----------
            parent : wx.window
                parent windows object to insert new row
            data : list
                data for the controls
            name : str
                name of the row object if not an enumeration
            """
            number = len(parent.GetChildren())

            m_cpu_panel = wx.Panel(parent, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
            b_sizer11 = wx.BoxSizer()

            if name:
                m_choice_key = wx.Choice(m_cpu_panel, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                         SystemViewGUI.i_lock_choices, 0)
                self.set_value(m_choice_key, name)
                b_sizer11.Add(m_choice_key, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)
            else:
                m_static_text10 = wx.StaticText(m_cpu_panel, wx.ID_ANY, str(number), wx.DefaultPosition, wx.DefaultSize,
                                                0)
                m_static_text10.Wrap(-1)
                b_sizer11.Add(m_static_text10, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)

            m_choice_key = wx.TextCtrl(m_cpu_panel, wx.ID_ANY, "", wx.DefaultPosition, wx.Size(140, -1))
            m_choice_key.AutoComplete(self.keytable)
            b_sizer11.Add(m_choice_key, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)
            m_choice_key.Bind(wx.EVT_TEXT, self.check_key_input)

            m_colour_active = wx.ColourPickerCtrl(m_cpu_panel, wx.ID_ANY, wx.BLACK, wx.DefaultPosition, wx.DefaultSize)
            b_sizer11.Add(m_colour_active, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)

            m_colour_passive = ExColourPickerCtrl(m_cpu_panel)
            b_sizer11.Add(m_colour_passive, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)

            m_button_remove = wx.Button(m_cpu_panel, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize,
                                        wx.BU_EXACTFIT)
            m_button_remove.SetBitmap(wx.ArtProvider.GetBitmap(wx.ART_DEL_BOOKMARK, wx.ART_BUTTON))
            b_sizer11.Add(m_button_remove, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 0)

            m_cpu_panel.SetSizer(b_sizer11)
            m_cpu_panel.Layout()
            b_sizer11.Fit(m_cpu_panel)

            sizer = parent.GetSizer()
            sizer.Add(m_cpu_panel, 0, wx.ALL, 5)

            self.Layout()

            m_button_remove.Bind(wx.EVT_LEFT_UP, self.remove_row)

            if len(data) < 2:
                return  # should never happen!

            self.set_value(m_choice_key, data[0])
            self.set_value(m_colour_active, data[1])
            if len(data) > 2:
                self.set_value(m_colour_passive, data[2])
            else:
                self.set_value(m_colour_passive, None)

        def set_data(self, data: Dict[str, Any], vendor: str) -> None:
            """
            set values of the controls

            Parameters
            ----------
            data : dict
                data to set controls; see also `preferences_struct()`
            vendor : str
                to set vendor specific menu entries
            """
            self.keytable = PluginsCoreGUI.i_keytable[vendor]
            self.effecttable = PluginsCoreGUI.i_effecttable[vendor]

            for child in self.m_panel_cpu.GetChildren():
                child.Destroy()
            for child in self.m_panel_lock.GetChildren():
                child.Destroy()

            for name, data2 in data["cpu_load"].items():
                self.add_row(self.m_panel_cpu, data2, "")
            for name, data2 in data["lock_keys"].items():
                self.add_row(self.m_panel_lock, data2, name)
            self.Fit()

        def get_data(self) -> Dict[str, Any]:
            """
            get values from the controls

            Return
            ------
            dict containing the control values
            """
            data = {"cpu_load": {}, "lock_keys": {}}
            for child in self.m_panel_cpu.GetChildren():
                row_data = []
                for elem in child.GetChildren():
                    if isinstance(elem, (wx.StaticText, wx.TextCtrl, wx.ColourPickerCtrl, ExColourPickerCtrl)):
                        dat = self.get_value(elem)
                        if dat:
                            row_data.append(dat)
                idx = row_data.pop(0)
                data["cpu_load"][idx] = row_data
            for child in self.m_panel_lock.GetChildren():
                row_data = []
                for elem in child.GetChildren():
                    if isinstance(elem, (wx.Choice, wx.TextCtrl, wx.ColourPickerCtrl, ExColourPickerCtrl)):
                        dat = self.get_value(elem)
                        if dat:
                            row_data.append(dat)
                idx = row_data.pop(0)
                data["lock_keys"][idx] = row_data
            return data

        def add_new_row(self, event: wx.Event) -> None:
            """
            event handler to add a new row

            Parameters
            ----------
            event : wx.event
                event object
            """
            button = event.GetEventObject()
            parent = button.GetNextSibling()
            if button.GetName() == "add_lock":
                active_entries = []
                for child in self.m_panel_lock.GetChildren():
                    entry = child.GetChildren()[0]
                    active_entries.append(self.get_value(entry))
                new_name = ""
                for name in SystemViewGUI.i_lock_choices:
                    if name not in active_entries:
                        new_name = name
                self.add_row(parent, [self.keytable[0], "0000FF", None], new_name)
                if len(self.m_panel_lock.GetChildren()) == len(SystemViewGUI.i_lock_choices):
                    self.m_add_lock.Disable()
            else:
                self.add_row(parent, [self.keytable[0], "ffffff", None], "")
            self.Layout()

        def remove_row(self, event: wx.Event) -> None:
            """
            event handler to remove a new row

            Parameters
            ----------
            event : wx.event
                event object
            """
            parent = event.GetEventObject()
            parent = parent.GetParent()
            grand_parent = parent.GetParent()
            parent.Destroy()
            # restore field nr on indexed rows
            childs = grand_parent.GetChildren()
            for idx, child in enumerate(childs):
                subchild = child.GetChildren()[0]
                if isinstance(subchild, wx.StaticText):
                    self.set_value(subchild, str(idx))
            if len(self.m_panel_lock.GetChildren()) != len(SystemViewGUI.i_lock_choices):
                self.m_add_lock.Enable()
            self.Layout()
except ImportError:
    print("wx framework not found.")
    wx = None
    PluginsCoreGUI = None
