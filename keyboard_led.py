#!/usr/bin/env python3
# coding=utf-8
"""Mainapp to setup the config for the Backgroud service."""

import glob
import importlib.util
import json
import os
import pathlib
import socket

from typing import Any, Dict, Optional

import wx

import keyboard_led_gui
from common.core_data import BASEDIR_SCRIPT, SERVICE_FOLDER, CONFIG_PATH

# this is necessary for wx, sorry
# pylint: disable=no-member
class KeyboardLED(keyboard_led_gui.KeyboardLEDGUI):
    """
    This is the config app to setup/edit the config file for the background service
    and it's plugins.
    """

    def __init__(self, parent: Optional[wx.Panel]) -> None:
        super().__init__(parent)

        connectors = []
        for filename in glob.glob1(os.path.join(BASEDIR_SCRIPT, SERVICE_FOLDER),
                                   "*_service_talker.py"):
            objname = filename[:-3]
            displayname = objname[:-15]
            obj = importlib.import_module(".%s" % objname, SERVICE_FOLDER)
            if hasattr(obj, 'DISPLAY_NAME'):
                displayname = obj.DISPLAY_NAME

            connectors.append(displayname)

        if importlib.util.find_spec('openrgb'):
            # pylint: disable=import-outside-toplevel   # can run without openrgb installed
            import services.openrgb_service_talker as ServiceTalker
            try:
                ServiceTalker.create_keyboard_def_file("127.0.0.1", 6742)
            except ConnectionRefusedError:
                if 'OpenRGB' in connectors:
                    connectors.remove('OpenRGB')

        self.init_config_menu(connectors)
        self.data["keyboard_service"] = self.get_connector_menu()

        pathlib.Path(CONFIG_PATH).mkdir(parents=True, exist_ok=True)
        self._load_plugins_defaults()

    def open_config(self, event: wx.Event) -> None:
        """
        load config if filepath is valid

        Parameters
        ----------
        event : wxEvent
            event object (not used)
        """
        filename = self.get_config_filepath()
        if not filename:
            return
        self.reload_config(event)

    def reload_config(self, event: wx.Event) -> None:
        """
        reload data and rebuild gui

        reload config from config file if filepath is valid or from the plugins preset

        Parameters
        ----------
        event : wxEvent
            event object (not used)
        """
        config_file = self.get_config_filepath()
        try:
            with open(config_file) as json_file:
                data = json.load(json_file)
                self.set_gui_data(data)
                self.set_plugins_data(data)
        except FileNotFoundError:
            answer = wx.MessageBox("Could not load config file!\nDo you want to reload "
                                   "the Plugin defaults?",
                                   "Load Error", wx.YES_NO)
            if answer == wx.YES:
                self._load_plugins_defaults()
            return

    def save_config(self, event: wx.KeyEvent) -> None:
        """
        save the data into the configfile

        save data into the previous opened configfile.
        if the button was ctrl-clicked it asked for a (new) filelocation (Save As)

        Parameters
        ----------
        event : wxEvent
            only used to check if ctrl key is pressed
        """
        if self.m_check_on_save.GetValue():
            if not self.check_duplicate_keys(False):
                return

        if event.ControlDown():
            with wx.FileDialog(self, "Save config file", wildcard="config files (*.json)|*.json",
                               style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as file_dialog:
                if file_dialog.ShowModal() == wx.ID_CANCEL:
                    return

                config_file = file_dialog.GetPath()
                self.set_config_filepath(config_file)
        else:
            config_file = self.get_config_filepath()
            if not config_file:
                wx.MessageBox(message="Need a config file first!", style=wx.OK | wx.ICON_ERROR)
                return

        dat = self.get_gui_data()
        self.data.update(dat)
        dat = self.get_plugins_data()
        self.data.update(dat)

        try:
            with open(config_file, 'w') as json_file:
                json.dump(self.data, json_file, indent=2)
        except FileNotFoundError:
            wx.MessageBox(message="Could not save config file!")
            return

        # signalling background service of the new settings
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.sendto(bytes("reload", "utf-8"), ("127.0.0.1", 14998))

    def check_duplicate_keys(self, notify_ok: bool) -> bool:
        """
        check all plugins data for duplicate keys. Ask user if this is ok if this occures

        Parameters
        ----------
        notify_ok : bool
            notify if result is ok
        Return
        ------
        bool : True if data is ok
        """
        key_dict = {}
        data = self.get_plugins_data()
        self._get_keys(data, key_dict)
        key_dict2 = {}
        for key, value in key_dict.items():
            if len(value) > 1:
                key_dict2[key] = value

        cnt = len(key_dict2)
        if cnt == 0:
            if notify_ok:
                wx.MessageBox("No duplicate keys found.", "Check OK", wx.OK)
            return True
        keys = ",".join(key_dict2.keys())
        if cnt == 1:
            text = "key '%s' has duplicate entries in %s!" % (keys, ",".join(key_dict2[keys]))
        else:
            text = "keys '%s' have duplicate entries!" % keys

        if notify_ok:
            wx.MessageBox(text, "Check Error", wx.OK)
            return False
        answer = wx.MessageBox("%s\nIgnore error?" % text, "Check Error", wx.YES_NO)
        return answer == wx.YES

    def _get_keys(self, data: Dict[str, Any], key_dict: Dict[str, Any], section: str = "") -> None:
        """
        fetch all used keyboard keys from the data structure

        Parameters
        ----------
        data : dict
            data to check for used keys
        key_dict : dict
            result dict containing all used keys
        section : str
            current section (plugin) as hint for the result
        """
        for key, value in data.items():
            if section:
                section1 = section
            else:
                section1 = key
            if isinstance(value, dict):
                self._get_keys(value, key_dict, section1)
            elif isinstance(value, list):
                for val2 in value:
                    if val2[:4] != "Key: ":
                        continue
                    if val2 in key_dict:
                        key_dict[val2].append(section)
                    else:
                        key_dict[val2] = [section]
            else:
                if value[:4] != "KEY_":
                    continue
                if value in key_dict:
                    key_dict[value].append(section)
                    continue
                key_dict[value] = [section]

    def check_data(self, event: wx.Event) -> None:
        self.check_duplicate_keys(True)

    def check_serverstring(self, event: wx.CommandEvent):
        obj: wx.TextCtrl = event.GetEventObject()
        text = obj.GetValue()
        text_ok = True
        server_str = text
        if server_str.count(':') > 1:
            text_ok = False
        elif server_str.count(':') == 1:
            server_str, port_str = server_str.split(':')
            if port_str:
                if not port_str.isnumeric():
                    text_ok = False
                else:
                    port = int(port_str)
                    if port == 0 or port > 65535:
                        text_ok = False
        if text_ok:
            obj.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
        else:
            obj.SetBackgroundColour(wx.Colour(255, 0, 0))


if __name__ == '__main__':
    app = wx.App()
    frame = KeyboardLED(parent=None)
    frame.Show()
    app.MainLoop()
