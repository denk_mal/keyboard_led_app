#!/usr/bin/env python3
# coding=utf-8
"""
helper class for service talker that uses named pipes
"""

import binascii
import fcntl
import logging
import os
import stat
import sys
import time

from typing import List

class NamedPipes:
    """handler for working with named pipes on linux"""

    def __init__(self, read_pipes: List[str], write_pipes: List[str]) -> None:
        """open the read and write pipes;

           the first pipe of the lists are opened for reading
           respectively writing.

        Parameters
        ----------
        read_pipes : list of pipe paths
            pipes for receiving data from service
        write_pipes : list of pipe paths
            pipes for sending data to the service
        """
        self.__thread = None
        if not isinstance(read_pipes, list):
            logging.critical("parameter for read_pipes must be a list!")
            sys.exit(1)
        if not isinstance(write_pipes, list):
            logging.critical("parameter for write_pipes must be a list!")
            sys.exit(0)
        self.__wait_for_pipes(read_pipes, write_pipes)
        self.__read_pipe = os.open(read_pipes[0], os.O_RDONLY | os.O_NONBLOCK)
        self.__write_pipe = os.open(write_pipes[0], os.O_RDWR)
        try:
            os.read(self.__read_pipe, 64)
        except BlockingIOError:
            pass
        flags = fcntl.fcntl(self.__read_pipe, fcntl.F_GETFL)
        flags &= ~os.O_NONBLOCK
        fcntl.fcntl(self.__read_pipe, fcntl.F_SETFL, flags)

    # pylint: disable=too-many-branches
    @staticmethod
    def __wait_for_pipes(read_pipes: List[str], write_pipes: List[str]) -> None:
        """check if pipes are existing and accessable.

        Parameters
        ----------
        read_pipes : list of pipe paths
            pipes for receiving data from service
        write_pipes : list of pipe paths
            pipes for sending data to the service
        """
        logging.debug("check for pipes...")
        for read_pipe in read_pipes:
            retry_cnt = 0
            while not os.path.exists(read_pipe):
                time.sleep(15)
                retry_cnt += 1
                if retry_cnt > 39:
                    logging.critical("app timed out (%s)", read_pipe)
                    sys.exit(1)
        for write_pipe in write_pipes:
            retry_cnt = 0
            while not os.path.exists(write_pipe):
                time.sleep(15)
                retry_cnt += 1
                if retry_cnt > 39:
                    logging.critical("app timed out (%s)", write_pipe)
                    sys.exit(1)
        pipe_state = True
        for read_pipe in read_pipes:
            if not stat.S_ISFIFO(os.stat(read_pipe).st_mode):
                pipe_state = False
                try:
                    if os.path.isfile(read_pipe):
                        os.remove(read_pipe)
                    else:
                        os.rmdir(read_pipe)
                except PermissionError:
                    logging.error("Can't remove file/dir '%s'", read_pipe)
        for write_pipe in write_pipes:
            if not stat.S_ISFIFO(os.stat(write_pipe).st_mode):
                pipe_state = False
                try:
                    if os.path.isfile(write_pipe):
                        os.remove(write_pipe)
                    else:
                        os.rmdir(write_pipe)
                except PermissionError:
                    logging.error("Can't remove file/dir '%s'", write_pipe)

        if not pipe_state:
            logging.critical("no regular pipes")
            sys.exit(1)
        logging.debug("pipes are up.")

    def close_pipes(self, cmd: str = "") -> None:
        """send a close command to the pipe.

        Parameters
        ----------
        cmd : str
            command for closing communication
        """
        data = binascii.unhexlify(cmd)
        os.write(self.__write_pipe, data)

    def execute_command(self, cmd: str) -> str:
        """write data to the write pipe and try to read result data.

        Parameters
        ----------
        cmd : str
            command send to the write pipe

        Return
        ------
        string
            the responds from the read pipe
        """
        data = binascii.unhexlify(cmd)
        os.write(self.__write_pipe, data)
        time.sleep(0.001)   # without this it may happen that results get lost
        result = os.read(self.__read_pipe, 64).hex()
        logging.debug("result: '%s'", result)
        return result
