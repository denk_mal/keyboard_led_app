#!/usr/bin/env python3
# coding=utf-8
"""
class for DAS Keyboard service talker
"""

import json
import logging
import os
import sys
import time

from typing import Dict

from services.named_pipes import NamedPipes
from services.service_talker import ServiceTalkerBase, LedAttrs

# get some base paths to load definition files
FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)

DISPLAY_NAME = "DAS"

class ServiceTalker(ServiceTalkerBase):
    """
    this is a communication layer between an application and the das-keyboard service.
    """

    def __init__(self, data: Dict) -> None:
        """Open the pipes to the backend service."""
        super().__init__(data)
        self.communication_id = ""
        self.max_id = 0
        self.profile = {}
        self.mapping = None
        tmpdir = "/tmp"
        pipe_read = os.path.join(tmpdir, 'daskeyboard_pipe_out')  # from service
        pipe_unsol = os.path.join(tmpdir, 'daskeyboard_pipe_unsolicited')
        pipe_write = os.path.join(tmpdir, 'daskeyboard_pipe_in')  # to service
        self.pipe_handler = NamedPipes([pipe_read, pipe_unsol], [pipe_write])

    @staticmethod
    def __convert_le_be__(in_str: str, size: int) -> str:
        """helperfunction; convert litte endian to big endian."""
        result = in_str
        while size > 1:
            result = ""
            for idx in range(0, len(in_str), size * 2):
                result += in_str[idx + size:idx + size * 2]
                result += in_str[idx:idx + size]
            size //= 2
            in_str = result
        return result

    def init_from_service(self) -> bool:
        """
        get information from current keyboard

        get some information from service like communication id, keyboard firmware version
        and firmware version.

        Return
        ------
        boolean
            true if no errors occured and the information are valid
        """
        data = self.pipe_handler.execute_command("0000000001000d")
        if not data:
            logging.fatal("no response from service.")
            sys.exit(1)
        data = data[14:]  # strip header
        logging.debug("received following data: '%s'", data)
        if len(data) <= 16:
            logging.fatal("no device connected.")
            return False
        self.communication_id = data[4:12]
        self.pid = self.__convert_le_be__(data[0:4], 2)
        self.firmware_version = "%d.%d.%d" % (int(data[12:14], 16),
                                             int(data[14:16], 16),
                                             int(data[16:18], 16))
        with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", "DK.json")) as json_file:
            mapping = json.load(json_file)
        if self.pid == '2020':
            self.mapping = mapping["5Q"]
        elif self.pid == '202B':
            self.mapping = mapping["X50Q"]
        elif self.pid == '2037':
            self.mapping = mapping["4Q"]
        if not self.mapping:
            return False
        # get highest key id
        size = 0
        for row in self.mapping['coords']:
            for key in row:
                if key == "NA":
                    continue
                idx = int(key)
                if idx > size:
                    size = idx
        # preset profile with passive black color, no active key settings
        self.max_id = size
        for idx in range(1, self.max_id + 1):
            key = str(idx)
            self.profile[key] = ["01%s000000" % key, "c0%s" % format(idx, '02x')]
        return True

    def end_transfer(self) -> None:
        """confirm previous settings as valid."""
        logging.info("data transfered.")
        cmd_prefix = self.communication_id + "0100"
        self.pipe_handler.execute_command(cmd_prefix + "ff")

    def close_service(self) -> None:
        """close communication with the das-keyboard."""
        self.pipe_handler.close_pipes("0000000001000d")

    def get_dimensions(self) -> dict:
        """get dimension of current keyboard (from json file)"""
        dimension = self.mapping["coords"]
        ret_val = {"width": len(dimension[0]), "height": len(dimension)}
        return ret_val

    def get_underglow_length(self) -> int:
        """get length of current keyboard underglow; not for DAS keyboards"""
        return 0

    def set_key(self, key: int,
                attribute: LedAttrs = LedAttrs(None, "", None, ""),
                is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a key


        This will set the effect and color for the passive and active state of the key.
        If the parameter is not set it will use the values from the profile. Calling this function
        only with the key parameter will 'restore' the profile settings for this key.

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that dosn't needs a color results in unforeseen effects!

        Parameters
        ----------
        key : int
            The key which should be changed
        attribute : LedAttrs, optional
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key; defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        cmd_prefix = self.communication_id + "0100"  # what stands this '0100' for ?
        cmd_ini = "40%s" % format(key, '02x')
        if attribute.passive_effect:
            cmd1 = "%s%s%s" % (format(attribute.passive_effect, '02x'),
                               format(key, '02x'), attribute.passive_color)
        else:
            cmd1 = self.profile[str(key)][0]
        if attribute.active_effect:
            cmd2 = "%s%s%s" % (format(attribute.active_effect, '02x'),
                               format(key, '02x'), attribute.active_color)
        else:
            cmd2 = self.profile[str(key)][1]
        if is_profile and cmd1:
            self.profile[str(key)] = [cmd1, cmd2]
        self.pipe_handler.execute_command(cmd_prefix + cmd_ini)
        self.pipe_handler.execute_command(cmd_prefix + cmd1)
        self.pipe_handler.execute_command(cmd_prefix + cmd2)

    def set_underglow(self, _pos: int,
                     attribute: LedAttrs = LedAttrs(None, "", None, ""),
                     is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a position on the underglow panel

        keyboard of DAS have no underglow.
        """
        return

    def set_underglow_column(self, column: int,
                             attribute: LedAttrs,
                             is_profile: bool = False) -> None:
        """
        translate the keyboard row to the underglow row and sets the color for all row leds

        keyboard of DAS have no underglow.
        """
        return

    def set_effect(self, effect: int = 2, color: str = "000000") -> None:
        """
        set effect for whole keyboard.

        The effect will not be permanently; A call to end_transfer() will drop the effect!

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that doesn't needs a color results in unforeseen effects!

        Parameters
        ----------
        effect : int, optional
            The effect of the keyboard; defaults to set static color
        color : str, optional
            The color of the effect (6 char hex string; RRGGBB); defaults to black
        """
        cmd_prefix = self.communication_id + "0100"  # what stands this '0100' for ?
        cmd1 = "%s%s" % (format(effect, '02x'), color)
        self.pipe_handler.execute_command(cmd_prefix + cmd1)

    def set_profile(self, effect: int, color: str) -> None:
        """
        set effect and color for all keys on the keyboard and store as profile

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that doesn't needs a color results in unforeseen effects!

        Parameters
        ----------
        effect : int
            The effect of the key (2 char hex string)
        color : str
            The color of the effect (6 char hex string; RRGGBB)
        """
        attribute = LedAttrs(effect, color)
        for idx in range(1, self.max_id + 1):
            self.set_key(idx, attribute, is_profile=True)

    def run(self) -> None:
        """
        worker thread

        """
        return

def main() -> None:
    """test function to show the possible settings

    shows the following settings (changes every 10 seconds):

        * static color; red
        * flashing color; green
        * breathing color: blue
        * color cycle
        * color cloud
    At the end the color will be set to static white color.

    """
    service_talker = ServiceTalker({})
    if not service_talker.init_from_service():
        print("something went wrong with the service communication!")
        return
    action_effect = service_talker.normalize_action("Effect Direct")
    service_talker.set_effect()
    service_talker.set_effect(action_effect, "ff0000")
    time.sleep(10)
    action_effect = service_talker.normalize_action("Effect Flashing")
    service_talker.set_effect()
    service_talker.set_effect(action_effect, "00ff00")
    time.sleep(10)
    action_effect = service_talker.normalize_action("Effect Breathing")
    service_talker.set_effect()
    service_talker.set_effect(action_effect, "0000ff")
    time.sleep(10)
    action_effect = service_talker.normalize_action("Effect Spectrum Cycle")
    service_talker.set_effect()
    service_talker.set_effect(action_effect)
    time.sleep(10)
    action_effect = service_talker.normalize_action("Effect Rainbow Wave")
    service_talker.set_effect()
    service_talker.set_effect(action_effect)
    time.sleep(10)
    action_effect = service_talker.normalize_action("Direct")
    service_talker.set_profile(action_effect, "ffffff")
    service_talker.end_transfer()
    service_talker.close_service()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s',
                        datefmt='%F %T',
                        level=logging.WARNING)
    main()
