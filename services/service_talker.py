#!/usr/bin/env python3
# coding=utf-8
"""
base class for keyboard specific service talkers
"""

import re
import threading
from typing import Any, Dict, NamedTuple, Optional

DISPLAY_NAME = "<unset>"

class LedAttrs(NamedTuple):
    """
    key meta data
    """
    passive_effect: int = None
    passive_color: str = ""
    active_effect: int = None
    active_color: str = ""


class ServiceTalkerBase(threading.Thread):
    """
    this is the base class for a communication layer.
    """

    def __init__(self, _data: Dict, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._stop_event = threading.Event()
        self.mapping = None
        self.pid = ""
        self.firmware_version = ""

    def init_from_service(self) -> bool:
        """
        get information from current keyboard

        get some information from service like communication id, keyboard firmware version
        and firmware version.

        Return
        ------
        boolean
            true if no errors occured and the information are valid
        """
        raise NotImplementedError

    def end_transfer(self) -> None:
        """confirm previous settings as valid."""
        raise NotImplementedError

    def close_service(self) -> None:
        """close communication with the das-keyboard."""
        raise NotImplementedError

    def get_dimensions(self) -> Dict:
        """get dimension of current keyboard"""
        raise NotImplementedError

    def get_underglow_length(self) -> int:
        """get length of current keyboard underglow"""
        raise NotImplementedError

    def set_key(self, key: int,
                attribute: LedAttrs = LedAttrs(None, "", None, ""),
                is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a key

        This will set the effect and color for the passive and active state of the key.
        If the parameter is not set it will use the values from the profile. Calling this function
        only with the key parameter will 'restore' the profile settings for this key.

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that dosn't needs a color results in unforeseen effects!

        Parameters
        ----------
        key : int
            The key which should be changed
        attribute : LedAttrs, optional
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key (2 char hex string); defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        raise NotImplementedError

    def set_underglow(self, pos: int,
                     attribute: LedAttrs = LedAttrs(None, "", None, ""),
                     is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a position on the underglow panel

        This will set the effect and color for the passive and active state of the underglow led.
        If the parameter is not set it will use the values from the profile. Calling this function
        only with the pos parameter will 'restore' the profile settings for the led of
        the underglow panel.

        Parameters
        ----------
        pos : int
            The pos of the underglow panel led which should be changed
        attribute : LedAttrs, optional
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key (2 char hex string); defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        raise NotImplementedError

    def set_underglow_column(self, column: int,
                             attribute: LedAttrs,
                             is_profile: bool = False) -> None:
        """
        translate the keyboard row to the underglow row and sets the color for all row leds

        Parameters
        ----------
        column : int
            column of the keyboard
        attribute : LedAttrs
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key (2 char hex string); defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        raise NotImplementedError

    def set_effect(self, effect: int, color: str = None) -> None:
        """
        set effect for whole keyboard.

        The effect will not be permanently; A call to end_transfer() will drop the effect!

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that doesn't needs a color results in unforeseen effects!

        Parameters
        ----------
        effect : int, optional
            The effect of the keyboard
        color : str, optional
            The color of the effect (6 char hex string; RRGGBB)
        """
        raise NotImplementedError

    def set_profile(self, effect: int, color: str) -> None:
        """
        set effect and color for all keys on the keyboard and store as profile

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that doesn't needs a color results in unforeseen effects!

        Parameters
        ----------
        effect : int
            The effect of the key
        color : str
            The color of the effect (6 char hex string; RRGGBB)
        """
        raise NotImplementedError

    def normalize_keycode(self, input_value: str) -> Optional[int]:
        """
        returns the keycode for the input value

        try to find out if the value is an id, a named key or a coordinate and returns the
        key code (from the json file) if it was a valid parameter

        Parameters
        ----------
        input_value :  str
            id, name or coordinate of the key

        Return
        ------
        int
            id of the key; None if the key was not valid
        """
        id_regex = re.compile(r'^1?\d?\d$')
        name_regex = re.compile(r'^Key: [\w ]+$')
        coord_regex = re.compile(r'^(\d?\d),(\d)$')
        input_value = input_value.strip()
        norm_value = None
        coords = self.mapping['coords']
        try:
            if id_regex.match(input_value):
                for row in coords:
                    if input_value in row:
                        norm_value = input_value
                        break
            elif name_regex.match(input_value):
                norm_value = self.mapping['names'][input_value]
            elif coord_regex.match(input_value):
                dimension = self.get_dimensions()
                x_pos = int(input_value.split(',')[0])
                y_pos = int(input_value.split(',')[1])
                if 0 <= x_pos < dimension['width'] and 0 <= y_pos < dimension['height']:
                    value = coords[y_pos][x_pos]
                    if value != "NA":
                        norm_value = value
        except KeyError:
            norm_value = None
        if norm_value:
            return int(norm_value)

        return None

    def normalize_action(self, action: str) -> Optional[int]:
        """
        returns the actioncode for the input value

        try to find out if the value is an id or a named action and returns the
        action code (from the json file) if it was a valid parameter

        Parameters
        ----------
        action : str
            id or name of the action

        Return
        ------
        int
            id of the action; None if the action was not valid
        """
        id_regex = re.compile(r'^[\da-fA-F]{2}$')
        name_regex = re.compile(r'^\w[\w ]+$')

        action = action.strip()
        norm_value = ""
        try:
            if id_regex.match(action):
                norm_value = action
            elif name_regex.match(action):
                norm_value = self.mapping['actions'][action]
        except KeyError:
            norm_value = None
        if norm_value:
            return int(norm_value)

        return None

    # threading.Thread functions
    def stop(self) -> None:
        """set flag to stop thread"""
        self._stop_event.set()

    def stopped(self) -> bool:
        """
        returns True if thread should be stopped

        Return
        ------
        bool
            True if thread should be ending
        """
        return self._stop_event.is_set()

    def run(self) -> None:
        """
        worker thread

        """
        raise NotImplementedError
