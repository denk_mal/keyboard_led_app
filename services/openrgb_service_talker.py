#!/usr/bin/env python3
# coding=utf-8
"""
class for OpenRGB Keyboard service talker
"""

import datetime
import json
import logging
import os
import sys
import time

from typing import Dict, Optional
import openrgb

from services.service_talker import ServiceTalkerBase, LedAttrs

# get some base paths to load definition files
FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)

DISPLAY_NAME = "OpenRGB"

TIME_SLICE = 0.5

# pylint: disable=line-too-long
# this is a static section because those information
# could not be taken from openrgb
UNDERGLOW_HUNDSMAN_ELITE_MATRIX = [
    # underglow field of keyboard
    [22,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14],
    [23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 15],
    [24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 16],
    [25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 17],
    [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41],
    # underglow field of palm rest
    [44, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 61],
    [45, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62],
    [46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 63]
]

SPECIAL_KEYS = [ "Keyboard", "Underglow" ]

def create_keyboard_def_file(address: str, port: int) -> None:
    """
    create/extend keyboard definition file for openrgb 'on the fly'
    with data from the openrgb service
    """
    filename = "OPEN_RGB.json"
    # use 'a+' as open flags to create json if it isn't existing
    with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", filename), 'a+') as json_file:
        try:
            json_file.seek(0)  # jump from the end of the file to the beginning for reading old
            mapping = json.load(json_file)
        except json.JSONDecodeError:
            mapping = {"vendor": "OpenRGB"}
        device_manager = openrgb.OpenRGBClient(address, port)
        devices = device_manager.devices
        for device in devices:
            if device.type != openrgb.utils.DeviceType.KEYBOARD:
                logging.debug("Skipping device %s %s)",
                    device.metadata.description, device.metadata.serial)
                devices.remove(device)
        for device in devices:
            name = device.name
            mapping[name] = create_device_def(device)

        # overwrite old content
        json_file.seek(0)
        # only to be sure that there are no parts of old content after the new dump
        json_file.truncate()
        json.dump(mapping, json_file, indent=2)


def create_device_def(device):
    """
    create the mapping for a single device

    Parameters
    ---------
    device : the device to scan

    Return
    ------
    mapping of the device
    """
    dev_mapping = {}
    zone = device.zones[0]
    key_matrix = []
    for row in zone.matrix_map:
        coord_row = []
        for field in row:
            if field is not None:
                coord_row.append(str(field))
            else:
                coord_row.append('NA')
        key_matrix.append(coord_row)
    dev_mapping['coords'] = key_matrix
    keys = {}
    for led in zone.leds:
        if led.name[:5] == "Key: ":
            keys[led.name] = str(led.id)
    for idx, name in enumerate(SPECIAL_KEYS):
        keys[name] = str(-1 - idx)
    dev_mapping['names'] = keys
    actions = {}
    for mode in device.modes:
        actions[mode.name] = str(mode.value)
    if 'Flashing' not in actions:
        actions['Flashing'] = "-1"
    dev_mapping['actions'] = actions
    return dev_mapping


# it seems that pylint counts the also the attributes from the parent
# pylint: disable=too-many-instance-attributes
class ServiceTalker(ServiceTalkerBase):
    """
    this is a communication layer between an application and the openrgb keyboard service.
    """

    def __init__(self, data: Dict) -> None:
        """Open the connection to the backend service."""
        super().__init__(data)
        self.device = None
        self.server = "localhost"
        self.port = 6742
        if 'openrgb_server' in data:
            server_str = data['openrgb_server']
            if server_str.count(':') == 0:
                self.server = server_str
            elif server_str.count(':') == 1:
                server_str, port_str = server_str.split(':')
                if server_str:
                    self.server = server_str
                if port_str.isnumeric():
                    port = int(port_str)
                    if 0 < port < 65536:
                        self.port = port
        self.mapping = {}
        self.profile = [[], []]
        self.key_actions = [[], []]
        self.flash_const = 1
        self.underglow_offset = 0
        self.underglow_length = 0
        self.underglow_profile = [[], []]
        self.underglow_action = [[], []]
        self._keyboard_underglow_translate = []

        # Create a DeviceManager. This is used to get specific devices
        try:
            device_manager = openrgb.OpenRGBClient(self.server, self.port)
        except ConnectionRefusedError:
            logging.fatal("opnergb server '%s:%s' connection refused!", self.server, self.port)
            return

        logging.debug("Found {%d OpenRGB device(s)", len(device_manager.devices))

        devices = device_manager.devices
        for device in devices:
            if device.type != openrgb.utils.DeviceType.KEYBOARD:
                logging.debug("Skipping device %s (%s)",
                              device.metadata.description,
                              device.metadata.serial)
                devices.remove(device)
        if not devices:
            logging.fatal("no valid openrgb keyboard device found!")
            return
        self.device = devices[0]
        if len(self.device.zones) > 1:
            self.underglow_offset = len(self.device.zones[0].leds)
            self.underglow_length = len(self.device.zones[1].leds)
        create_keyboard_def_file(self.server, self.port)

    @staticmethod
    def __convert_color__(color: str) -> tuple or None:
        """
        converts a rgb-hexstring to an equivalent tuple

        Parameters
        ---------
        color : hexstring of the color

        Return
        ------
        tuple of three ints (red, green, blue)
        """
        if not color:
            return None
        if len(color) < 6:
            return None
        color_r = int(color[0:2], 16)
        color_g = int(color[2:4], 16)
        color_b = int(color[4:6], 16)
        return color_r, color_g, color_b

    def __get_device_value(self, key: str) -> tuple or None:
        """returns the info from the device if it exists or None"""
        if hasattr(self.device, key):
            return getattr(self.device, key)
        if hasattr(self.device.metadata, key):
            return getattr(self.device.metadata, key)
        return None

    def init_from_service(self) -> bool:
        """
        get information from current keyboard

        get some information from service like communication id and keyboard firmware version.

        Parameters
        ----------

        Return
        ------
        boolean
            true if no errors occured and the information are valid
        """
        if not self.device:
            logging.fatal("no device connected.")
            return False
        self.pid = self.__get_device_value('name')
        self.firmware_version = self.__get_device_value('version')
        filename = "OPEN_RGB.json"
        with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", filename)) as json_file:
            mapping = json.load(json_file)
            if self.pid in mapping:
                self.mapping = mapping[self.pid]
        if not self.mapping:
            logging.error("no section for '%s' in the keyboard definition file", self.pid)
            return False
        # get highest key id
        # preset profile with passive black color, no active key settings
        for layer in range(2):
            for x_pos in range(self.device.zones[0].mat_height):
                self.profile[layer].append([])
                self.key_actions[layer].append([])
                for _y in range(self.device.zones[0].mat_width):
                    self.profile[layer][x_pos].append((0, 0, 0))
                    self.key_actions[layer][x_pos].append(None)
            for _x_pos in range(self.underglow_length):
                self.underglow_profile[layer].append((0, 0, 0))
                self.underglow_action[layer].append(None)
        # calculate translation table between keyboard and underglow section
        len_row_kb = len(self.profile[0][0])
        len_row_ug = len(UNDERGLOW_HUNDSMAN_ELITE_MATRIX[0]) - 1
        for x_row in range(len_row_kb):
            row_id = (x_row * len_row_ug) + (len_row_kb // 2)
            row_id = row_id // (len_row_kb - 1)
            self._keyboard_underglow_translate.append(row_id)
        self.flash_const = self.normalize_action('Flashing')
        self.start()
        return True

    def end_transfer(self):
        """confirm previous settings as valid. (simply)"""
        # time.sleep(TIME_SLICE)

    def close_service(self) -> None:
        """close communication with the keyboard."""
        self.stop()
        self.join()

    def get_dimensions(self) -> dict:
        """get dimension of current keyboard (from keyboard)"""
        ret_val = {"width": len(self.profile[0][0]), "height": len(self.profile[0])}
        return ret_val

    def get_underglow_length(self) -> int:
        """get length of current keyboard underglow"""
        return self.underglow_length

    def set_key(self, key: int,
                attribute: LedAttrs = LedAttrs(None, "", None, ""),
                is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a key

        This will set the effect and color for the passive and active state of the key.
        If the parameter is not set it will use the values from the profile. Calling this function
        only with the key parameter will 'restore' the profile settings for this key.

        Parameters
        ----------
        key : int
            The key which should be changed
        attribute : LedAttrs, optional
            The effect of the key; defaults to current profile
            The color of the key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key; defaults to current profile
            The second color of the key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """

        if not key:
            return
        column_id = -1
        row_id = -1
        key_str = str(key)
        coords = self.mapping['coords']
        for row in coords:
            if key_str in row:
                column_id = row.index(key_str)
                row_id = coords.index(row)
                break

        if row_id == -1 or column_id == -1:
            if not self.set_all_keys(attribute, key):
                logging.warning("key with id '%s' is not defined", key)
            return
        key_color = self.__convert_color__(attribute.passive_color)
        bg_color = self.__convert_color__(attribute.active_color)
        if is_profile:
            self.profile[0][row_id][column_id] = key_color
            if attribute.passive_effect != self.flash_const:
                self.profile[1][row_id][column_id] = key_color
            else:
                self.profile[1][row_id][column_id] = bg_color
        else:
            self.key_actions[0][row_id][column_id] = key_color
            if attribute.passive_effect != self.flash_const:
                self.key_actions[1][row_id][column_id] = key_color
            else:
                self.key_actions[1][row_id][column_id] = bg_color

    def set_underglow(self, pos: int,
                     attribute: LedAttrs = LedAttrs(None, "", None, ""),
                     is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a position on the underglow panel

        This will set the effect and color for the passive and active state of the underglow led.
        If the parameter is not set it will use the values from the profile. Calling this function
        only with the pos parameter will 'restore' the profile settings for the led of
        the underglow panel.

        Parameters
        ----------
        pos : int
            The pos of the underglow panel led which should be changed
        attribute : LedAttrs, optional
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key (2 char hex string); defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        if pos >= self.underglow_length:
            logging.warning("pos '%s' is not defined", pos)
            return
        bg_color = self.__convert_color__(attribute.active_color)
        key_color = self.__convert_color__(attribute.passive_color)
        if is_profile:
            self.underglow_profile[0][pos] = key_color
            if attribute.passive_effect != self.flash_const:
                self.underglow_profile[1][pos] = key_color
            else:
                self.underglow_profile[1][pos] = bg_color
        else:
            self.underglow_action[0][pos] = key_color
            if attribute.passive_effect != self.flash_const:
                self.underglow_action[1][pos] = key_color
            else:
                self.underglow_action[1][pos] = bg_color

    def set_underglow_column(self, column: int,
                             attribute: LedAttrs,
                             is_profile: bool = False) -> None:
        """
        translate the keyboard row to the underglow row and sets the color for all row leds

        Parameters
        ----------
        column : int
            column of the keyboard
        attribute : LedAttrs
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key (2 char hex string); defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        if column > len(self._keyboard_underglow_translate):
            return
        column = self._keyboard_underglow_translate[column]
        for row in UNDERGLOW_HUNDSMAN_ELITE_MATRIX:
            y_pos = row[column]
            if y_pos != -1:
                self.set_underglow(y_pos, attribute, is_profile)

    def set_effect(self, effect: int = 1, color: str = "000000") -> None:
        """
        set effect for whole keyboard. (Not on OpenRGB)

        Parameters
        ----------
        effect : int, optional (not used)
            The effect of the keyboard; defaults to set static color
        color : str, optional (not used)
            The color of the effect (6 char hex string; RRGGBB); defaults to black
        """
        return  # not on openrgb

    def set_profile(self, effect: int, color: str) -> None:
        """
        set effect and color for all keys on the keyboard and store as profile

        Parameters
        ----------
        effect : int
            The effect of the key
        color : str
            The color of the effect (6 char hex string; RRGGBB)
        """
        new_color = self.__convert_color__(color)
        color_black = self.__convert_color__("000000")
        for row_id in range(len(self.profile[0])):
            for column_id in range(len(self.profile[0][0])):
                self.profile[0][row_id][column_id] = new_color
                if effect != self.flash_const:
                    self.profile[1][row_id][column_id] = new_color
                else:
                    self.profile[1][row_id][column_id] = color_black
        for pos in range(self.underglow_length):
            self.underglow_profile[0][pos] = new_color
            if effect != self.flash_const:
                self.underglow_profile[1][pos] = new_color
            else:
                self.underglow_profile[1][pos] = color_black

    def set_all_keys(self, attribute: LedAttrs, special_idx: int) -> None:
        """
        set effect and color for all leds

        Parameters
        ----------
        attribute : LedAttrs
            The effect of the unpressed key; defaults to current profile
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key (2 char hex string); defaults to current profile
            The color of the pressed key (6 char hex string; RRGGBB); defaults to current profile
        special_idx : int
            select the area (zone)
        """
        fg_color = self.__convert_color__(attribute.passive_color)
        bg_color = self.__convert_color__(attribute.active_color)
        effect = attribute.passive_effect
        index = -1 - special_idx
        if index == 0:
            for row_id in range(len(self.profile[0])):
                for column_id in range(len(self.profile[0][0])):
                    self.key_actions[0][row_id][column_id] =fg_color
                    if effect != self.flash_const:
                        self.key_actions[1][row_id][column_id] = fg_color
                    else:
                        self.key_actions[1][row_id][column_id] = bg_color
        elif index == 1:
            for pos in range(self.underglow_length):
                self.underglow_action[0][pos] = fg_color
                if effect != self.flash_const:
                    self.underglow_action[1][pos] = fg_color
                else:
                    self.underglow_action[1][pos] = bg_color
        else:
            return False
        return True

    def normalize_keycode(self, input_value: str) -> Optional[int]:
        """
        returns the keycode for the input value

        try to find out if the value is an id, a named key or a coordinate and returns the
        key code (from the json file) if it was a valid parameter

        Parameters
        ----------
        input_value :  str
            id, name or coordinate of the key

        Return
        ------
        int
            id of the key; None if the key was not valid
        """
        retval = super().normalize_keycode(input_value)
        if input_value in SPECIAL_KEYS:
            norm_value = self.mapping['names'][input_value]
            if norm_value:
                return int(norm_value)
        return retval

    def stop(self):
        """set flag to stop thread"""
        self._stop_event.set()

    def stopped(self):
        """
        returns True if thread should be stopped

        Return
        ------
        bool
            True if thread should be ending
        """
        return self._stop_event.is_set()

    # pylint: disable=too-many-branches
    def run(self):
        """
        worker thread

        writes the profile every 500 mSec to the openrgb keyboard driver

        """
        while not self.stopped():
            if not self.device:
                try:
                    device_manager = openrgb.OpenRGBClient(self.server, self.port)
                    devices = device_manager.devices
                    for device in devices:
                        if device.type != openrgb.utils.DeviceType.KEYBOARD:
                            logging.debug("Skipping device %s (%s)",
                                device.metadata.description, device.metadata.serial)
                            devices.remove(device)
                    if devices:
                        self.device = devices[0]
                except ConnectionRefusedError:
                    logging.fatal("opnergb server '%s:%s' connection refused!",
                                  self.server, self.port)
                    time.sleep(60)
                continue

            default_mode = self.device.modes[0].name
            self.device.set_mode(default_mode)
            time.sleep(TIME_SLICE)
            layer = 0
            if datetime.datetime.now().microsecond >= 500000:
                layer = 1
            # confirm previous settings as valid.
            matrix_map = self.device.zones[0].matrix_map
            height = len(self.profile[layer])
            width = len(self.profile[layer][0])
            for y_pos in range(width):
                for x_pos in range(height):
                    pos = matrix_map[x_pos][y_pos]
                    if pos is not None:
                        color = self.key_actions[layer][x_pos][y_pos]
                        if color is None:
                            color = self.profile[layer][x_pos][y_pos]
                        self.device.colors[pos] = \
                            openrgb.utils.RGBColor(color[0], color[1], color[2])
            for pos in range(self.underglow_length):
                color = self.underglow_action[layer][pos]
                if color is None:
                    color = self.underglow_profile[layer][pos]
                self.device.colors[pos + self.underglow_offset] = openrgb.utils.RGBColor(color[0], color[1], color[2])
            try:
                self.device.show()
            except openrgb.utils.OpenRGBDisconnected:
                self.device = None


def main() -> None:
    """test function to show the possible settings

    shows the following settings (changes every 10 seconds):

        * static color; red
        * flashing color; green
    At the end the color will be set to static white color.

    """
    service_talker = ServiceTalker({})
    if not service_talker.init_from_service():
        print("something went wrong with the service communication!")
        return
    # service_talker.create_keyboard_def_file()
    action_effect = service_talker.normalize_action("Direct")
    action_effect_flash = service_talker.normalize_action("Flashing")
    service_talker.set_profile(action_effect, "ff0000")
    time.sleep(10)
    service_talker.set_profile(action_effect_flash, "00ff00")
    time.sleep(10)
    action = LedAttrs(action_effect_flash, "00ff00", action_effect_flash, "ff0000")
    service_talker.set_profile(action_effect, "ffffff")
    key = service_talker.normalize_keycode("Underglow")
    service_talker.set_key(key, action)
    time.sleep(30)
    key = service_talker.normalize_keycode("Underglow")
    service_talker.set_key(key)
    time.sleep(0.1)
    service_talker.set_profile(action_effect, "ffffff")
    time.sleep(0.1)
    service_talker.end_transfer()
    service_talker.close_service()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s',
                        datefmt='%F %T',
                        level=logging.DEBUG)
    main()
