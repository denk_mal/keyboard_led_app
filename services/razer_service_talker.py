#!/usr/bin/env python3
# coding=utf-8
"""
class for Razer Keyboard service talker
"""

import datetime
import json
import logging
import os
import sys
import time

from typing import Dict, Tuple, Optional

from openrazer.client import DeviceManager

from services.service_talker import ServiceTalkerBase, LedAttrs

# get some base paths to load definition files
FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)

DISPLAY_NAME = "Razer"

TIME_SLICE = 0.1


class ServiceTalker(ServiceTalkerBase):
    """
    this is a communication layer between an application and the razer keyboard service.
    """

    def __init__(self, _data) -> None:
        """Open the connection to the backend service."""
        super().__init__(_data)
        self.mapping = None
        self.profile = [[], []]
        self.key_actions = [[], []]
        # Create a DeviceManager. This is used to get specific devices
        device_manager = DeviceManager()

        logging.debug("Found %d Razer devices", len(device_manager.devices))

        devices = device_manager.devices
        for device in devices:
            if not device.fx.advanced:
                logging.debug("Skipping device %s (%s)", device.name, device.serial)
                devices.remove(device)
        if not devices:
            logging.fatal("no valid razer device found!")
            self.device = None
        else:
            self.device = devices[0]

    @staticmethod
    def __convert_color__(color: str) -> Optional[Tuple[int, int, int]]:
        """
        converts a rgb-hexstring to an equivalent List of ints

        Parameters
        ---------
        color : hexstring of the color

        Return
        ------
        list of three ints (red, green, blue)
        """
        if not color:
            return None
        if len(color) < 6:
            return None
        color_r = int(color[0:2], 16)
        color_g = int(color[2:4], 16)
        color_b = int(color[4:6], 16)
        return color_r, color_g, color_b

    def __get_device_value(self, key: str) -> Optional[str]:
        """returns the info from the device if it exists or None"""
        if key in self.device.capabilities:
            if self.device.capabilities[key]:
                return getattr(self.device, key)
        return None

    def init_from_service(self) -> bool:
        """
        get information from current keyboard

        get some information from service like communication id and keyboard firmware version.

        Parameters
        ----------

        Return
        ------
        boolean
            true if no errors occured and the information are valid
        """
        if not self.device:
            logging.fatal("no device connected.")
            return False
        self.pid = self.__get_device_value('name')
        self.firmware_version = self.__get_device_value('firmware_version')
        modelname = self.pid[6:]
        with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", "Razer.json")) as json_file:
            mapping = json.load(json_file)
            self.mapping = mapping[modelname]
        if not self.mapping:
            return False
        # get highest key id
        # preset profile with passive black color, no active key settings
        for layer in range(2):
            for x_pos in range(self.device.fx.advanced.rows):
                self.profile[layer].append([])
                self.key_actions[layer].append([])
                for _y in range(self.device.fx.advanced.cols):
                    self.profile[layer][x_pos].append((0, 0, 0))
                    self.key_actions[layer][x_pos].append(None)
        self.start()
        return True

    def end_transfer(self) -> None:
        """confirm previous settings as valid. (simply)"""
        time.sleep(TIME_SLICE)

    def close_service(self) -> None:
        """close communication with the keyboard."""
        self.stop()
        self.join()

    def get_dimensions(self) -> Dict:
        """get dimension of current keyboard (from keyboard)"""
        ret_val = {"width": len(self.profile[0][0]), "height": len(self.profile[0])}
        return ret_val

    def get_underglow_length(self) -> int:
        """get length of current keyboard underglow; not for device.fx driver"""
        return 0

    def set_key(self, key: int,
                attribute: LedAttrs = LedAttrs(None, "", None, ""),
                is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a key

        This will set the effect and color for the passive and active state of the key.
        If the parameter is not set it will use the values from the profile. Calling this function
        only with the key parameter will 'restore' the profile settings for this key.

        Parameters
        ----------
        key : int
            The key which should be changed
        attribute : LedAttrs, optional
            The effect of the key; defaults to current profile
            The color of the key (6 char hex string; RRGGBB); defaults to current profile
            The effect of the pressed key; defaults to current profile
            The second color of the key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """

        column_id = -1
        row_id = -1
        key_str = str(key)
        coords = self.mapping['coords']
        for row in coords:
            if key_str in row:
                column_id = row.index(key_str)
                row_id = coords.index(row)
                break

        if row_id == -1 or column_id == -1:
            logging.warning("key with id '%s' is not defined", key)
            return
        key_color = self.__convert_color__(attribute.passive_color)
        bg_color = self.__convert_color__(attribute.active_color)
        if is_profile:
            self.profile[0][row_id][column_id] = key_color
            if attribute.passive_effect != 3:
                self.profile[1][row_id][column_id] = key_color
            else:
                self.profile[1][row_id][column_id] = bg_color
        else:
            self.key_actions[0][row_id][column_id] = key_color
            if attribute.passive_effect != 3:
                self.key_actions[1][row_id][column_id] = key_color
            else:
                self.key_actions[1][row_id][column_id] = bg_color

    def set_underglow(self, _pos: int,
                     attribute: LedAttrs = LedAttrs(None, "", None, ""),
                     is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a position on the underglow panel

        device.fx driver have no underglow.
        """
        return

    def set_underglow_column(self, column: int,
                             attribute: LedAttrs,
                             is_profile: bool = False) -> None:
        """
        translate the keyboard row to the underglow row and sets the color for all row leds

        device.fx driver have no underglow.
        """
        return

    def set_effect(self, effect: int = 1, color: str = "000000") -> None:
        """
        set effect for whole keyboard. (Not on Razer Keyboards)

        The effect will not be permanently; A call to end_transfer() will drop the effect!

        Parameters
        ----------
        effect : int, optional (not used)
            The effect of the keyboard; defaults to set static color
        color : str, optional (not used)
            The color of the effect (6 char hex string; RRGGBB); defaults to black
        """
        return  # not on razer

    def set_profile(self, effect: int, color: str) -> None:
        """
        set effect and color for all keys on the keyboard and store as profile

        Parameters
        ----------
        effect : int
            The effect of the key
        color : str
            The color of the effect (6 char hex string; RRGGBB)
        """
        for row_id in range(len(self.profile[0])):
            for column_id in range(len(self.profile[0][0])):
                self.profile[0][row_id][column_id] = self.__convert_color__(color)
                if effect != 3:
                    self.profile[1][row_id][column_id] = self.__convert_color__(color)
                else:
                    self.profile[1][row_id][column_id] = self.__convert_color__("000000")

    def run(self) -> None:
        """
        writes the profile every 100 mSec to the openRazer keyboard driver

        """
        while not self.stopped():
            time.sleep(TIME_SLICE)
            if datetime.datetime.now().microsecond < 500000:
                layer = 0
            else:
                layer = 1
            # confirm previous settings as valid.
            for x_pos in range(len(self.profile[layer])):
                for y_pos in range(len(self.profile[layer][0])):
                    if self.key_actions[layer][x_pos][y_pos]:
                        self.device.fx.advanced.matrix[x_pos, y_pos] = \
                            self.key_actions[layer][x_pos][y_pos]
                    else:
                        self.device.fx.advanced.matrix[x_pos, y_pos] = \
                            self.profile[layer][x_pos][y_pos]
            self.device.fx.advanced.draw()


def main() -> None:
    """test function to show the possible settings

    shows the following settings (changes every 10 seconds):

        * static color; red
        * Flashing color; green
    At the end the color will be set to static white color.

    """
    service_talker = ServiceTalker({})
    if not service_talker.init_from_service():
        print("something went wrong with the service communication!")
        return
    key = service_talker.normalize_keycode('Key: Number Pad 3')
    service_talker.set_key(key)
    action_effect = service_talker.normalize_action("Direct")
    service_talker.set_profile(action_effect, "ff0000")
    time.sleep(10)
    action_effect = service_talker.normalize_action("Flashing")
    service_talker.set_profile(action_effect, "00ff00")
    time.sleep(10)
    action_effect = service_talker.normalize_action("Direct")
    service_talker.set_profile(action_effect, "ffffff")
    service_talker.end_transfer()
    service_talker.close_service()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s',
                        datefmt='%F %T',
                        level=logging.WARNING)
    main()
