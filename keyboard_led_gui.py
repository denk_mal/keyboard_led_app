#!/usr/bin/env python3
# coding=utf-8
"""Subclass of MainFrame, which translate standard functions to the gui framework (wxPython)."""

import importlib
import glob
import logging
import os
from typing import Any, Dict, List, Type

import wx

import main_frame
from common.core_data import BASEDIR_SCRIPT,\
    CONFIG_PATH, PLUGIN_FOLDER, SECTION_KEYBOARD_LED, SUB_SECTION_DISABLED, SUB_SECTION_CHECK_SAVED
from common.core_data import init_class
from common.plugins_core import PluginsCoreGUI

SETTINGS_KEYBOARD_LED = {SUB_SECTION_CHECK_SAVED: True, SUB_SECTION_DISABLED: []}

# this is necessary for wx, sorry
# pylint: disable=no-member
class KeyboardLEDGUI(main_frame.MainFrame):
    """
    wrapper class to mediate between application and gui framework

    this wrapper interacts between the functional (child) class and the
    gui framework and implements only gui related tasks.

    Here wxWidget (wxPython) is used as gui framework

    """

    def __init__(self, parent: wx.Window) -> None:
        super().__init__(parent)
        self.plugins: Dict[str, PluginsCoreGUI] = {}
        self.preset: Dict[str, Any] = {}
        self.data: Dict[str, Any] = {}
        self.vendor = ""
        self.openrgb_server = "localhost:6742"
        self.m_file_picker.SetInitialDirectory(CONFIG_PATH)

    def _load_plugins_defaults(self) -> None:
        """
        load plugins from plugin folder, get presets if defined by the plugins and
        create notebook panes
        """
        for filename in glob.glob1(os.path.join(BASEDIR_SCRIPT, PLUGIN_FOLDER), "*.py"):
            name = filename[:-3]
            if name.startswith('__'):
                continue
            logging.debug("try to initialize plugin: %s", name)
            obj = importlib.import_module(".%s" % name, 'plugins')
            if hasattr(obj, 'preferences_struct'):
                # two calls instead of a deep copy(!)
                self.preset[name] = obj.preferences_struct()
                self.data[name] = obj.preferences_struct()
            self.add_tab(name, self.preset[name])
            self.m_list_plugins.Append(name)
        self.m_list_plugins.SetCheckedStrings(list(self.plugins.keys()))

    def set_gui_data(self, data: Dict) -> None:
        """
        set values of the gui elements

        data : dict
            data for the gui
        """
        self.vendor = data["keyboard_service"]
        if 'openrgb_server' in data:
            server = data['openrgb_server']
            if server:
                self.openrgb_server = server
        self.set_connector_menu(self.vendor)
        if SECTION_KEYBOARD_LED in data:
            data = data[SECTION_KEYBOARD_LED]
            new_data = {}
            for key, value in SETTINGS_KEYBOARD_LED.items():
                try:
                    new_data[key] = data[key]
                except KeyError:
                    new_data[key] = value
        else:
            new_data = SETTINGS_KEYBOARD_LED

        self.m_openrgb_server.SetValue(self.openrgb_server)
        self.m_check_on_save.SetValue(new_data[SUB_SECTION_CHECK_SAVED])
        self.m_list_plugins.SetCheckedStrings(list(self.plugins.keys()))
        for item in new_data[SUB_SECTION_DISABLED]:
            idx = self.m_list_plugins.FindString(item)
            if idx != wx.NOT_FOUND:
                self.m_list_plugins.Check(idx, False)

    def get_gui_data(self) -> Dict:
        """
        get values from the gui elements

        Return
        ------
        dict of all gui values
        """
        data = {SECTION_KEYBOARD_LED: {}, 'keyboard_service': self.get_connector_menu(),
                'openrgb_server': self.m_openrgb_server.GetValue()}
        data_k = data[SECTION_KEYBOARD_LED]
        data_k[SUB_SECTION_CHECK_SAVED] = self.m_check_on_save.GetValue()
        data_k[SUB_SECTION_DISABLED] = list(set(self.m_list_plugins.GetStrings()) -
                                            set(self.m_list_plugins.GetCheckedStrings()))
        return data

    def set_plugins_data(self, data: Dict) -> None:
        """
        set values of the plugin controls

        Parameters
        ----------
        data : dict
            data for the plugin controls
        """
        for plugin, data2 in data.items():
            if plugin in self.plugins:
                try:
                    self.plugins[plugin].set_data(data2, self.vendor)
                except KeyError:
                    logging.error("plugin '%s' load data failed!", plugin)
                    self.plugins[plugin].set_data(self.preset[plugin], self.vendor)

    def get_plugins_data(self) -> Dict:
        """
        get values from the plugin controls

        Return
        ------
        dict of all plugin values
        """
        data = {}
        for plugin_name, plugin in self.plugins.items():
            data[plugin_name] = plugin.get_data()
        return data

    def add_tab(self, tabname: str, data: Dict) -> None:
        """
        create or renew the content of a tab described by `data`

        Parameters
        ----------
        tabname : str
            name of the plugin
        data : dict
            config data for the plugin (see also `preferences_struct()` of the plugin)
        """
        self.vendor = self.get_connector_menu()
        for idx in range(self.m_plugins_nb.GetPageCount()):
            if tabname == self.m_plugins_nb.GetPageText(idx):
                self.m_plugins_nb.DeletePage(idx)
                break
        clazz: Type[PluginsCoreGUI] = init_class(tabname, "GUI")
        if clazz:
            instance = clazz(self.m_plugins_nb)
            instance.set_data(data, self.vendor)
            self.plugins[tabname] = instance
            self.m_plugins_nb.InsertPage(self.m_plugins_nb.GetRowCount(), instance, tabname)

    def init_config_menu(self, item_list: List[str]) -> None:
        """
        init the connector menu

        Parameters
        ----------
        item_list : list of str
            content for the connector menu
        """
        self.m_connector.AppendItems(item_list)
        self.m_connector.SetSelection(0)

    def get_config_filepath(self) -> str:
        """get the file path from the config file control"""
        return self.m_file_picker.GetPath()

    def set_config_filepath(self, path: str) -> None:
        """set the file path from the config file control"""
        self.m_file_picker.SetPath(path)

    def get_connector_menu(self) -> str:
        """get the selected connector"""
        return self.m_connector.GetString(self.m_connector.GetSelection())

    def set_connector_menu(self, name: str) -> None:
        """set the selected connector"""
        idx = self.m_connector.FindString(name)
        if idx != wx.NOT_FOUND:
            self.m_connector.SetSelection(idx)

    def ctrl_handle(self, event: wx.KeyEvent) -> None:
        """
        handle pressing of the ctrl key

        add '...' to the save button to show extra fuctionallity if the
        save button is ctrl-clicked

        Parameters
        ----------
        event : wxEvent
            event to get the state of the ctrl key
        """
        label = self.m_save_button.GetLabel()
        if event.ControlDown():
            if not label.endswith("..."):
                self.m_save_button.SetLabel("%s..." % label)
        else:
            if label.endswith("..."):
                self.m_save_button.SetLabel(label[:-3])
        event.Skip()

    def change_vendor(self, event: wx.Event) -> None:
        """
        get data from (plugin)controls and set it with a new vendor (for vendor specific menus)
        """
        self.vendor = self.get_connector_menu()
        data = self.get_plugins_data()
        self.set_plugins_data(data)

    def quit_config(self, event: wx.Event) -> None:
        """close app"""
        self.Close()
