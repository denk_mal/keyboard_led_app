#!/usr/bin/env python3
# coding=utf-8
"""background service for driving the led keyboard effects"""

import argparse
import importlib
import glob
import json
import logging
import os
import pathlib
import socket
import sys
import threading
import time
from multiprocessing.dummy import Pool

from typing import Any, Dict, List, Optional, Type

from common.core_data import BASEDIR_SCRIPT, CONFIG_PATH, PLUGIN_FOLDER,\
    SECTION_KEYBOARD_LED, SUB_SECTION_DISABLED
from common.core_data import init_class
from common.plugins_core import PluginsCore
from services.service_talker import ServiceTalkerBase

TIME_SLICE = 5


class KeyboardLEDService:
    """ This class connects to the led keyboard service, loads the config file,
        loads the plugins and schedules the plugins.
    """
    def __init__(self, config_file: pathlib.Path, preaction: bool = False) -> None:
        """get a connection to the das-keyboard service and loads the config file.
        the config file is a simple container for config parts
        of the plugins e.g like this
        '''
        {
          "keyboard_service": "DAS",
          "profile": {
            ...
          },
          mail_watcher: {
            ...
          }
        }
        '''

        Parameters
        ----------
        config_file : str

        """
        logging.info("use configfile: '%s'", config_file)
        self.config_file = config_file
        self.socket_port = 14998
        self.data = {}
        self.__service_handler: Optional[ServiceTalkerBase] = None
        self.__schedule_tasks: List[PluginsCore] = []
        self.close_app = False

        self.reload()

        # The thread that ables the listen for UDP packets is loaded
        self.listen_udp = threading.Thread(target=self.get_remote_cmd)
        self.listen_udp.start()

        if preaction:
            action = self.__service_handler.normalize_action("EFFECT_COLOR_CLOUD")
            self.__service_handler.set_effect(action)
            # not necessary; it's only to show on the keyboard that the process has started
            time.sleep(30)

    def reload(self) -> None:
        """
        stop and unload all loaded plugins and the keyboard service talker if they are active
        start the keyboard service talker and all plugins
        """
        with open(self.config_file) as json_file:
            self.data = json.load(json_file)

        # unload plugins and service
        for task in self.__schedule_tasks:
            task.stop()
            if task.is_alive():
                task.join()
        self.__schedule_tasks.clear()
        if self.__service_handler:
            self.__service_handler.close_service()

        # load service
        service_talker = "services.%s_service_talker" % self.data.get("keyboard_service", "razer")
        try:
            module = importlib.import_module(service_talker)
        except ImportError:
            module = importlib.import_module(service_talker.lower())
        clazz = getattr(module, "ServiceTalker")
        self.__service_handler: ServiceTalkerBase = clazz(self.data)

        if not self.__service_handler.init_from_service():
            logging.critical("could not connect service")
            sys.exit(1)
        self.init_plugins()

    def init_plugins(self) -> None:
        """scan plugin folder for .py files, load this module and initialise it.
            All plugins are added to the schedule task table that will be called repeatedly.
        """
        try:
            disabled = self.data[SECTION_KEYBOARD_LED][SUB_SECTION_DISABLED]
        except KeyError:
            disabled = []
        for filename in glob.glob1(os.path.join(BASEDIR_SCRIPT, PLUGIN_FOLDER), "*.py"):
            name = filename[:-3]
            if name in disabled:
                continue
            logging.debug("try to initialize plugin: %s", name)
            clazz: Type[PluginsCore] = init_class(name)
            if clazz:
                try:
                    if name in self.data:
                        instance = clazz(self.__service_handler, self.data[name])
                        self.__schedule_tasks.append(instance)
                    else:
                        logging.error("no valid data in the config file for plugin '%s'", name)
                except RuntimeError as err:
                    logging.error("init of plugin '%s' failed with message : %s", name, err)

    def get_remote_cmd(self) -> None:
        """
        UDP commands for listening
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(('127.0.0.1', self.socket_port))
        while True:
            cmd, _addr = sock.recvfrom(1024)
            logging.debug("received remote cmd from socket ('%s')", cmd)
            if cmd == b"close":
                self.close_app = True
                break
            if cmd == b"reload":
                self.reload()

    @staticmethod
    def _trigger_event(task: PluginsCore) -> Dict:
        return task.schedule_event()

    def event_loop(self) -> None:
        """call the schedule_event() function by using the multiprocessing function of python."""
        try:
            while True:
                pool = Pool(4)  # handle up the worker processes for the plugins
                # start all plugins and wait for all processes to finished
                results = pool.map(self._trigger_event, self.__schedule_tasks)
                key_was_set = False
                # iterate over the results and set any keys
                for items in results:
                    for key, values in items.items():
                        self.__service_handler.set_key(key, *values)
                        key_was_set = True
                if key_was_set:
                    self.__service_handler.end_transfer()
                pool.close()  # close all plugin threads
                pool.join()  # wait until all plugins stopped
                if self.close_app:
                    for task in self.__schedule_tasks:
                        task.stop()
                        if task.is_alive():
                            task.join()
                    self.__service_handler.close_service()
                    break
                time.sleep(TIME_SLICE)  # wait TIME_SLICE sec for the next round
        except KeyboardInterrupt:
            for task in self.__schedule_tasks:
                task.stop()
            self.__service_handler.close_service()


def parse_arguments() -> Any:
    """
    parse arguments from the command line
    """
    config_file = os.path.join(CONFIG_PATH, 'keyboard_led.json')
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default=config_file,
                        help="change default configuration location")
    parser.add_argument("-p", "--preaction", action="store_true",
                        help="execute a ligining feedback on the keyboard before startung up")
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="increase verbosity")
    args = parser.parse_args()
    level = logging.ERROR
    if args.verbosity == 1:
        level = logging.WARNING
    elif args.verbosity == 2:
        level = logging.INFO
    elif args.verbosity >= 3:
        level = logging.DEBUG
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s',
                        datefmt='%F %T',
                        level=level)
    logging.info("Set logging.level to %s", logging.getLevelName(level))
    return args


def get_lock(process_name):
    """
    allow single instance only
    """
    # Without holding a reference to our socket somewhere it gets garbage
    # collected when the function exits
    get_lock.lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    try:
        # The null byte (\0) means the socket is created
        # in the abstract namespace instead of being created
        # on the file system itself.
        # Works only in Linux
        get_lock.lock_socket.bind('\0' + process_name)
    except socket.error:
        print("'%s' is already running. Will exit now!" % process_name)
        sys.exit()


def main() -> None:
    """
    start the service
    """
    app_name = os.path.basename(sys.argv[0])
    get_lock(app_name)
    args = parse_arguments()
    config_file = pathlib.Path(args.config).expanduser()
    main_object = KeyboardLEDService(config_file, args.preaction)
    main_object.event_loop()


if __name__ == '__main__':
    main()
