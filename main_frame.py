# -*- coding: utf-8 -*-

###########################################################################
# Python code generated from the wxFormBuilder generated code template
# There are (hopefully) only 2 differences
# 1. code is reformatted (by PyCharm)
# 2. some PEP8 enhancements (remove setting of default parameters
#    and comment styling)
###########################################################################

import wx
import wx.xrc
import wx.adv


###########################################################################
# Class MainFrame
###########################################################################


class MainFrame(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Keyboard-LED", pos=wx.DefaultPosition,
                          size=wx.Size(800, 600), style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer1 = wx.BoxSizer(wx.VERTICAL)

        self.main_panel = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                   wx.BORDER_SUNKEN | wx.TAB_TRAVERSAL)
        b_sizer5 = wx.BoxSizer(wx.VERTICAL)

        self.m_static_text1 = wx.StaticText(self.main_panel, wx.ID_ANY, u"Config Tool for Keyboard-LED",
                                            wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTER_HORIZONTAL)
        self.m_static_text1.Wrap(-1)

        self.m_static_text1.SetFont(
            wx.Font(12, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Sans"))

        b_sizer5.Add(self.m_static_text1, 0, wx.ALL | wx.EXPAND, 5)

        self.main_panel.SetSizer(b_sizer5)
        self.main_panel.Layout()
        b_sizer5.Fit(self.main_panel)
        b_sizer1.Add(self.main_panel, 0, wx.ALL | wx.EXPAND, 5)

        fg_sizer1 = wx.FlexGridSizer(0, 2, 0, 0)
        fg_sizer1.AddGrowableCol(1)
        fg_sizer1.SetFlexibleDirection(wx.BOTH)
        fg_sizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_static_text2 = wx.StaticText(self, wx.ID_ANY, u"Config Path:", wx.DefaultPosition, wx.DefaultSize,
                                            wx.ALIGN_CENTER_HORIZONTAL)
        self.m_static_text2.Wrap(-1)

        fg_sizer1.Add(self.m_static_text2, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_file_picker = wx.FilePickerCtrl(self, wx.ID_ANY, wx.EmptyString, u"Load config file",
                                               u"config file (*.json)|*.json", wx.DefaultPosition, wx.DefaultSize,
                                               wx.FLP_DEFAULT_STYLE | wx.FLP_FILE_MUST_EXIST | wx.FLP_OPEN)
        fg_sizer1.Add(self.m_file_picker, 0, wx.ALL | wx.EXPAND, 5)

        b_sizer1.Add(fg_sizer1, 0, wx.EXPAND, 5)

        sb_sizer1 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Plugins"), wx.VERTICAL)

        self.m_plugins_nb = wx.Notebook(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                        wx.NB_MULTILINE)
        self.m_core_prefs = wx.Panel(self.m_plugins_nb, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        gb_sizer1 = wx.GridBagSizer(0, 0)
        gb_sizer1.SetFlexibleDirection(wx.BOTH)
        gb_sizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_static_text3 = wx.StaticText(self.m_core_prefs, wx.ID_ANY, u"Keyboard Connector", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_static_text3.Wrap(-1)

        gb_sizer1.Add(self.m_static_text3, wx.GBPosition(0, 0), wx.GBSpan(1, 1), wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.m_connector = wx.Choice(self.m_core_prefs, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, [], wx.CB_SORT)
        self.m_connector.SetSelection(0)
        gb_sizer1.Add(self.m_connector, wx.GBPosition(0, 1), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        self.m_static_text4 = wx.StaticText(self.m_core_prefs, wx.ID_ANY, u"OpenRGB Connection", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.m_static_text4.Wrap(-1)

        gb_sizer1.Add(self.m_static_text4, wx.GBPosition(1, 0), wx.GBSpan(1, 1), wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        self.m_openrgb_server = wx.TextCtrl(self.m_core_prefs, wx.ID_ANY, u"localhost:6742", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        gb_sizer1.Add(self.m_openrgb_server, wx.GBPosition(1, 1), wx.GBSpan(1, 1), wx.ALL | wx.EXPAND, 5)

        self.m_staticline2 = wx.StaticLine(self.m_core_prefs, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                           wx.LI_HORIZONTAL)
        gb_sizer1.Add(self.m_staticline2, wx.GBPosition(2, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        self.m_check_on_save = wx.CheckBox(self.m_core_prefs, wx.ID_ANY, u"Check data on Save", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.m_check_on_save.SetValue(True)
        gb_sizer1.Add(self.m_check_on_save, wx.GBPosition(3, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        self.m_list_plugins = wx.CheckListBox(self.m_core_prefs, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, [],
                                              wx.LB_SORT)
        gb_sizer1.Add(self.m_list_plugins, wx.GBPosition(4, 0), wx.GBSpan(1, 2), wx.ALL | wx.EXPAND, 5)

        gb_sizer1.AddGrowableCol(1)
        gb_sizer1.AddGrowableRow(4)

        self.m_core_prefs.SetSizer(gb_sizer1)
        self.m_core_prefs.Layout()
        gb_sizer1.Fit(self.m_core_prefs)
        self.m_plugins_nb.AddPage(self.m_core_prefs, u"Core", True)
        self.m_help_page = wx.Panel(self.m_plugins_nb, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer4 = wx.BoxSizer(wx.VERTICAL)

        self.m_static_text5 = wx.StaticText(self.m_help_page, wx.ID_ANY, u"Help", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text5.Wrap(-1)

        self.m_static_text5.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        b_sizer4.Add(self.m_static_text5, 0, wx.ALL, 5)

        self.m_help_text = wx.StaticText(self.m_help_page, wx.ID_ANY,
                                         u"Dies ist ein kleiner Hilfstest, der ein paar Dinge erklärt.\n"
                                         u"So z.B was es mit den Checkboxen neben den Farbauswählen auf sich hat.\n",
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_help_text.Wrap(-1)

        b_sizer4.Add(self.m_help_text, 1, wx.ALL | wx.EXPAND, 5)

        self.m_static_text6 = wx.StaticText(self.m_help_page, wx.ID_ANY, u"About", wx.DefaultPosition, wx.DefaultSize,
                                            0)
        self.m_static_text6.Wrap(-1)

        self.m_static_text6.SetFont(
            wx.Font(wx.NORMAL_FONT.GetPointSize(), wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD,
                    False, wx.EmptyString))

        b_sizer4.Add(self.m_static_text6, 0, wx.ALL, 5)

        self.m_static_text7 = wx.StaticText(self.m_help_page, wx.ID_ANY, u"Frank Nießen\n©2020-2022",
                                            wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text7.Wrap(-1)

        b_sizer4.Add(self.m_static_text7, 0, wx.ALL | wx.EXPAND, 5)

        self.m_hyperlink2 = wx.adv.HyperlinkCtrl(self.m_help_page, wx.ID_ANY, u"Source auf Gitlab",
                                                 u"http://www.gitlab.com", wx.DefaultPosition, wx.DefaultSize,
                                                 wx.adv.HL_DEFAULT_STYLE)
        b_sizer4.Add(self.m_hyperlink2, 0, wx.ALL, 5)

        self.m_help_page.SetSizer(b_sizer4)
        self.m_help_page.Layout()
        b_sizer4.Fit(self.m_help_page)
        self.m_plugins_nb.AddPage(self.m_help_page, u"Help", False)

        sb_sizer1.Add(self.m_plugins_nb, 10, wx.EXPAND | wx.ALL, 5)

        b_sizer1.Add(sb_sizer1, 20, wx.EXPAND, 5)

        b_sizer2 = wx.BoxSizer()

        self.m_reload_button = wx.Button(self, wx.ID_ANY, u"Reload", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer2.Add(self.m_reload_button, 0, wx.ALL, 5)

        self.m_check_button = wx.Button(self, wx.ID_ANY, u"Check", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer2.Add(self.m_check_button, 0, wx.ALL, 5)

        b_sizer2.Add((0, 0), 1, wx.EXPAND, 5)

        self.m_save_button = wx.Button(self, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer2.Add(self.m_save_button, 0, wx.ALL, 5)

        self.m_quit_button = wx.Button(self, wx.ID_ANY, u"Quit", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer2.Add(self.m_quit_button, 0, wx.ALL, 5)

        b_sizer1.Add(b_sizer2, 1, wx.EXPAND, 5)

        self.SetSizer(b_sizer1)
        self.Layout()

        self.Centre()

        # Connect Events
        self.main_panel.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.main_panel.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_file_picker.Bind(wx.EVT_FILEPICKER_CHANGED, self.open_config)
        self.m_file_picker.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.m_file_picker.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_plugins_nb.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.m_plugins_nb.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_connector.Bind(wx.EVT_CHOICE, self.change_vendor)
        self.m_connector.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.m_connector.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_openrgb_server.Bind(wx.EVT_TEXT, self.check_serverstring)
        self.m_reload_button.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.m_reload_button.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_reload_button.Bind(wx.EVT_LEFT_UP, self.reload_config)
        self.m_check_button.Bind(wx.EVT_LEFT_UP, self.check_data)
        self.m_save_button.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.m_save_button.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_save_button.Bind(wx.EVT_LEFT_UP, self.save_config)
        self.m_quit_button.Bind(wx.EVT_KEY_DOWN, self.ctrl_handle)
        self.m_quit_button.Bind(wx.EVT_KEY_UP, self.ctrl_handle)
        self.m_quit_button.Bind(wx.EVT_LEFT_UP, self.quit_config)

    def __del__(self):
        pass

    # Virtual event handlers, overide them in your derived class
    def ctrl_handle(self, event):
        event.Skip()

    def open_config(self, event):
        event.Skip()

    def change_vendor(self, event):
        event.Skip()

    def check_serverstring(self, event):
        event.Skip()

    def reload_config(self, event):
        event.Skip()

    def check_data(self, event):
        event.Skip()

    def save_config(self, event):
        event.Skip()

    def quit_config(self, event):
        event.Skip()
